#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <algorithm>
#include <iomanip>
#include "includes/typesOfInstances.h"
#include "includes/inputFunctions.h"
#include "includes/timer.h"
#include "includes/allocationMatrices.h"
#include "includes/outputFunctions.h"

constexpr int  POZITIVNA_BESKONACNOST = 1e9;

// ADJUST: Adjust parameter values.
constexpr int VELICINA_POPULACIJE = 33;
constexpr int  ELITA = 31;
constexpr int VELICINA_TURNIRA = 5;
constexpr double  VEROVATNOCA_MUTACIJE = 0.3549;
constexpr double  MAX_VREME_GA  = 1000;

int brojCvorova;
std::vector<std::vector<double> > cene;
int p;
double alfa;
// If gama and delta are not equal to 1, include appropriate declaration
// double gama = 1, delta = 1;

class Jedinka {
public:
	// Set of hubs. In the SA implementation this is a vector called "habovi".
	std::vector<int> kod;
	double fitnes;
	
	Jedinka() {
		kod.resize(p);
		for (int j = 0; j < p; j++) {
			kod[j] = std::rand() % brojCvorova;
		}
		
		fitnes = fitnes_funkcija();
	}
	
	// This function is the same as vrednost_resenja() in the SA implementation.
	double fitnes_funkcija() {
		std::vector<std::vector<double> > dist;

		inicijalizacija_pocetne_matrice(dist);
		pronadji_najkrace_puteve(dist);
		double max = pronadji_max_cenu_najkracih_puteva(dist);

		return max;
	}

private:
	// Time complexity: O(n^2*p), where n = brojCvorova.
	void inicijalizacija_pocetne_matrice(std::vector<std::vector<double> >& dist) {
		/*
		In this case gama = 1 and delta = 1. Logic for constructing the initial dist matrix:
		dist[i][j] = infinity,              i, j are not hubs                       (neither)
		dist[i][j] = alfa * cene[i][j],     i and j are both hubs                   (both)
		dist[i][j] = cene[i][j],            i != j, exactly one of i, j is a hub    (exactly one)
		*/
		/*
		If (gama != 1) and/or (delta != 1), constructing the initial dist matrix:
		dist[i][j] = infinity,                 i, j are not hubs                   (neither)
		dist[i][j] = alfa * cene[i][j],        i and j are both hubs               (both)
		dist[i][j] = delta * cene[i][j],       when i is a hub                     (only i is a hub)
		dist[i][j] = gama * cene[i][j],        when j is a hub                     (only j is a hub)
		*/

		dist.resize(brojCvorova);
		for (int i = 0; i < brojCvorova; i++){
			dist[i].resize(brojCvorova);
			for (int j = 0; j < brojCvorova; j++){
				auto nadjiIuHabovi = std::find(std::begin(kod), std::end(kod), i);
				auto nadjiJuHabovi = std::find(std::begin(kod), std::end(kod), j);

				if (nadjiIuHabovi == std::end(kod) && nadjiJuHabovi == std::end(kod))
					dist[i][j] = POZITIVNA_BESKONACNOST;
				else if (nadjiIuHabovi != std::end(kod) && nadjiJuHabovi != std::end(kod))
					dist[i][j] = alfa * cene[i][j];
				else
					dist[i][j] = cene[i][j];

				// If (gama != 1) and/or (delta != 1), the last else statement should be REPLACED with:
				#if 0
				else if (nadjiIuHabovi == std::end(habovi))
					dist[i][j] = delta * cene[i][j];
				else
					dist[i][j] = gama * cene[i][j];
				#endif

			}
		}
	}

	// Modified Floyd - Warshall algorithm to find costs of shortest paths via hubs.
	// Time complexity: O(p*n^2), where n = brojCvorova
	void pronadji_najkrace_puteve(std::vector<std::vector<double> >& dist) {

		for (int brojac = 0; brojac < p; brojac++)
			for (int i = 0; i < brojCvorova; i++)
				for (int j = 0; j < brojCvorova; j++)
					dist[i][j] = std::min(dist[i][j], dist[i][kod[brojac]] + dist[kod[brojac]][j]);
	}

	// Time complexity: O(n^2), where n = brojCvorova.
	double pronadji_max_cenu_najkracih_puteva(std::vector<std::vector<double> >& dist) {
		double max = 0;

		for (int i = 0; i < brojCvorova; i++)
			for (int j = 0; j < brojCvorova; j++)
				max = std::max(max, dist[i][j]);

		return max;
	}
	
};

std::vector<Jedinka> populacija;
std::vector<Jedinka> novaPopulacija;

int selekcija(int velicinaTurnira) {
	double min = POZITIVNA_BESKONACNOST;
	int pobednik = -1;
	
	for (int i = 0; i < velicinaTurnira; i++) {
		int k = std::rand() % VELICINA_POPULACIJE;
		
		if (populacija[k].fitnes < min) {
			min = populacija[k].fitnes;
			pobednik = k;
		}
	}
	
	return pobednik;
}

// ADJUST: Choose which function ukrstanje() should be used (3 options).
// One-point crossover.
void ukrstanje(Jedinka& roditelj1,
				Jedinka& roditelj2,
				Jedinka& potomak1,
				Jedinka& potomak2) {
	int indeks = std::rand() % p;
	
	for (int i = 0; i < indeks; i++) {
		potomak1.kod[i] = roditelj1.kod[i];
		potomak2.kod[i] = roditelj2.kod[i];
	}
	
	for (int i = indeks; i < p; i++) {
		potomak1.kod[i] = roditelj1.kod[i];
		potomak2.kod[i] = roditelj2.kod[i];
	}
}

#if 0
// Two-point crossover.
void ukrstanje(Jedinka& roditelj1,
				Jedinka& roditelj2,
				Jedinka& potomak1,
				Jedinka& potomak2) {
	// Pick two random indexes.
	int prviIndeks = std::rand() % p;
	int drugiIndeks;
	do {
		drugiIndeks = std::rand() % p;
	} while (prviIndeks == drugiIndeks);

	// Store the picked values so that: prviIndeks < drugiIndeks.
	if (drugiIndeks < prviIndeks) {
		int pomocna = prviIndeks;
		prviIndeks = drugiIndeks;
		drugiIndeks = pomocna;
	}
	
	for (int i = 0; i < prviIndeks; i++) {
		potomak1.kod[i] = roditelj1.kod[i];
		potomak2.kod[i] = roditelj2.kod[i];
	}
	
	for (int i = prviIndeks; i < drugiIndeks; i++) {
		potomak1.kod[i] = roditelj2.kod[i];
		potomak2.kod[i] = roditelj1.kod[i];
	}

	for (int i = drugiIndeks; i < p; i++) {
		potomak1.kod[i] = roditelj1.kod[i];
		potomak2.kod[i] = roditelj2.kod[i];
	}
}
#endif

#if 0
// Uniform crossover.
void ukrstanje(Jedinka& roditelj1,
				Jedinka& roditelj2,
				Jedinka& potomak1,
				Jedinka& potomak2) {
	
	for (int i = 0; i < p; i++) {
		int roditeljZaGenPotomka1 = std::rand() % 2;

		if (roditeljZaGenPotomka1 == 1) {
			potomak1.kod[i] = roditelj1.kod[i];
			potomak2.kod[i] = roditelj2.kod[i];
		} else {
			potomak1.kod[i] = roditelj2.kod[i];
			potomak2.kod[i] = roditelj1.kod[i];
		}
	}
}
#endif

void mutacija(Jedinka& jedinka) {
	int k;
	
	for (int i = 0; i < p; i++) {
		if (std::rand() % 100 > VEROVATNOCA_MUTACIJE)
			continue;
			
		do {
			k = std::rand() % brojCvorova;
		} while (std::find(jedinka.kod.begin(), jedinka.kod.end(), k) != jedinka.kod.end());
		jedinka.kod[i] = k;
	}
}

bool uporedi(Jedinka i1, Jedinka i2) {
	return i1.fitnes < i2.fitnes;
}

int main(int argc, char** argv) {
	unsigned instance;
	
	srand(time(NULL));

	std::cout << "\nUnesite broj 1 ako se program pokrece nad CAB instancama, broj 2 ukoliko se pokrece nad AP instancama ili broj 3 ukoliko su u pitanju URAND instance: ";
	std::cin >> instance;
	std::cout << "\n";

	static_cast<tipInstance>(instance);

	// Read instance file.
	if (instance == CAB) 
		ucitajCAB(argv[1], brojCvorova, p, alfa, cene);
	else if (instance == AP)
		ucitajAP(argv[1], brojCvorova, p, alfa, cene);
	else if (instance == URAND)
		ucitajURAND(argv[1], brojCvorova, p, alfa, cene);
	else {
		std::cout << "Nekorektan unos.\n";
		return 0;
	}

	Stoperica stopericaGA;	// Start timer.
	
	populacija.reserve(VELICINA_POPULACIJE);
	novaPopulacija.reserve(VELICINA_POPULACIJE);
	for (int i = 0; i < VELICINA_POPULACIJE; i++) {
		populacija.emplace_back(Jedinka());
		novaPopulacija.emplace_back(Jedinka());
	}
	std::sort(populacija.begin(), populacija.end(), uporedi);

	double najboljaVrednost = populacija[0].fitnes;
	std::cout << "\nTrenutna najbojla vrednost: " << std::fixed << std::setprecision(2) << najboljaVrednost << "\t";
	double protekloVremeGA = stopericaGA.izracunaj_proteklo_vreme();
	std::cout << "Vreme: " << protekloVremeGA << " ms \n";

	int neparanBrojJedinkiZaUkrstanjeIMutaciju = (VELICINA_POPULACIJE - ELITA) % 2;
	while (protekloVremeGA < MAX_VREME_GA) {

		for (int i = 0; i < ELITA; i++)
			novaPopulacija[i] = populacija[i];
		

		// TODO: Change this when you set the ELITE parameter. Delete "neparanBrojJedinkiZaUkrstanjeIMutaciju" parameter.
		if (neparanBrojJedinkiZaUkrstanjeIMutaciju) {
			for (int i = ELITA; i < VELICINA_POPULACIJE-1; i+=2) {
				int i1 = selekcija(VELICINA_TURNIRA);
				int i2 = selekcija(VELICINA_TURNIRA);
				
				ukrstanje(populacija[i1], populacija[i2], novaPopulacija[i], novaPopulacija[i + 1]);
				
				mutacija(novaPopulacija[i]);
				mutacija(novaPopulacija[i + 1]);
				
				novaPopulacija[i].fitnes = novaPopulacija[i].fitnes_funkcija();
				novaPopulacija[i + 1].fitnes = novaPopulacija[i + 1].fitnes_funkcija();
			}

			mutacija(novaPopulacija[VELICINA_POPULACIJE-1]);
			novaPopulacija[VELICINA_POPULACIJE-1].fitnes = novaPopulacija[VELICINA_POPULACIJE-1].fitnes_funkcija();
		} else {
			for (int i = ELITA; i < VELICINA_POPULACIJE; i+=2) {
				int i1 = selekcija(VELICINA_TURNIRA);
				int i2 = selekcija(VELICINA_TURNIRA);
				
				ukrstanje(populacija[i1], populacija[i2], novaPopulacija[i], novaPopulacija[i + 1]);
				
				mutacija(novaPopulacija[i]);
				mutacija(novaPopulacija[i + 1]);
				
				novaPopulacija[i].fitnes = novaPopulacija[i].fitnes_funkcija();
				novaPopulacija[i + 1].fitnes = novaPopulacija[i + 1].fitnes_funkcija();
			}
		}
		
		populacija = novaPopulacija;
		std::sort(populacija.begin(), populacija.end(), uporedi);

		if (populacija[0].fitnes < najboljaVrednost) {
			najboljaVrednost = populacija[0].fitnes;
			
			std::cout << "Trenutna najbojla vrednost: " << std::fixed << std::setprecision(2) << najboljaVrednost << "\t";
			double vremeTrazenjaNoveNajboljeVrednosti = stopericaGA.izracunaj_proteklo_vreme();
			std::cout << "Vreme: " << vremeTrazenjaNoveNajboljeVrednosti << " ms \n";
		}

		protekloVremeGA = stopericaGA.izracunaj_proteklo_vreme();
	}

	// Output time spent, NOT including time for calculating allocation matrices.
	double protekloVremePreAB = stopericaGA.izracunaj_proteklo_vreme();
	std::cout << "Ukupno vreme pre racunanja A i B matrice: " << protekloVremePreAB << " ms \n";

	// Calculate allocation matrices.
	std::vector<std::vector<double> > dist;
	std::vector<std::vector<int> > A; // First allocation matrix.
	std::vector<std::vector<int> > B; // Second allocation matrix.
	inicijalizacija_pocetnih_matrica_alokacije(dist, 
											populacija[0].kod /* habovi */,
											A,
											B,
											alfa,
											cene,
											brojCvorova,
											POZITIVNA_BESKONACNOST);
	popuni_matrice_alokacije(dist,
							populacija[0].kod /* habovi */,
							p,
							A,
							B,
							brojCvorova,
							POZITIVNA_BESKONACNOST);

	// Output time spent, including time for calculating allocation matrices.
	double protekloVreme = stopericaGA.izracunaj_proteklo_vreme();
	std::cout << "Ukupno vreme: " << protekloVreme << " ms \n";

	// Output results.
	std::ofstream outputFajl("results/GA/small_instances/AP102-rezultati.txt");	// ADJUST: Adjust output file name.
	ispisi_vrednost_resenja_i_habove(outputFajl,
									populacija[0].fitnes /* vrednostResenja */,
									populacija[0].kod /* habovi */,
									p);
	ispisi_matrice_alokacije(outputFajl, A , B, brojCvorova, POZITIVNA_BESKONACNOST);
	outputFajl.close();

	return 0;
}
