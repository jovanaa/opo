#pragma once

#include <fstream>
#include <vector>

void ispisi_vrednost_resenja_i_habove(std::ofstream& outputFajl,
                                    double vrednostResenja,
                                    std::vector<int> habovi,
                                    int p);
void ispisi_matrice_alokacije(std::ofstream& outputFajl,
                            std::vector<std::vector<int> > A,
							std::vector<std::vector<int> > B,
							int brojCvorova,
                    		double POZITIVNA_BESKONACNOST);