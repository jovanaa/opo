#pragma once

#include <vector>

void ucitajCAB(const char* nazivFajla,
			int& brojCvorova,
			int& p,
			double& alfa,
			std::vector<std::vector<double> >& cene);
void ucitajAP(const char* nazivFajla,
			int& brojCvorova,
			int& p,
			double& alfa,
			std::vector<std::vector<double> >& cene);
void ucitajURAND(const char* nazivFajla,
				int& brojCvorova,
				int& p,
				double& alfa,
				std::vector<std::vector<double> >& cene);