#include "outputFunctions.h"
#include <fstream>
#include <iomanip>
#include <vector>

void ispisi_vrednost_resenja_i_habove(std::ofstream& outputFajl,
                                    double vrednostResenja,
                                    std::vector<int> habovi,
                                    int p) {
    outputFajl <<  std::fixed << std::setprecision(2) << "Vrednost resenja: " << vrednostResenja << std::endl;

	outputFajl << "\nZa habove treba uzeti cvorove: ";
	for (int i = 0; i < p-1; i++) {
		outputFajl << habovi[i] << ", ";
	}
	outputFajl << habovi[p-1] << "\n";
}

void ispisi_matricu(std::ofstream& outputFajl,
                    std::vector<std::vector<int> > matrica,
                    int brojCvorova,
                    double POZITIVNA_BESKONACNOST) {
	for (int i = 0; i < brojCvorova; i++) {
		for (int j = 0; j < brojCvorova; j++) {
			if (matrica[i][j] != POZITIVNA_BESKONACNOST)
				outputFajl << matrica[i][j] << " ";
			else
				outputFajl << "x ";
		}
	outputFajl << "\n";
	}
}

void ispisi_matrice_alokacije(std::ofstream& outputFajl,
                            std::vector<std::vector<int> > A,
							std::vector<std::vector<int> > B,
							int brojCvorova,
                    		double POZITIVNA_BESKONACNOST) {
    outputFajl << "\nA ('x' oznacava polje koje nema relevantnu vrednost):\n";
    ispisi_matricu(outputFajl, A, brojCvorova, POZITIVNA_BESKONACNOST);

    outputFajl << "\nB ('x' oznacava polje koje nema relevantnu vrednost):\n";
    ispisi_matricu(outputFajl, B, brojCvorova, POZITIVNA_BESKONACNOST);
}