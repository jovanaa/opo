#include "timer.h"
#include <iostream>
#include <chrono>

Stoperica::Stoperica() {
	pocetniTrenutak = std::chrono::high_resolution_clock::now();
}

double Stoperica::izracunaj_proteklo_vreme() {
	auto krajnjiTrenutak = std::chrono::high_resolution_clock::now();

	auto pocetak = std::chrono::time_point_cast<std::chrono::microseconds>(pocetniTrenutak).time_since_epoch().count();
	auto kraj = std::chrono::time_point_cast<std::chrono::microseconds>(krajnjiTrenutak).time_since_epoch().count();

	auto trajanje = kraj - pocetak;			// microseconds
	double trajanje_ms = trajanje * 0.001;	// milliseconds

	return trajanje_ms;
}

#if 0
void Stoperica::ispisi_proteklo_vreme(double trajanje_ms) {
	std::cout << "Vreme: " << trajanje_ms << " ms \n";
}
#endif