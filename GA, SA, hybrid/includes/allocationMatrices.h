#pragma once

#include <vector>

void inicijalizacija_pocetnih_matrica_alokacije(std::vector<std::vector<double> >& dist,
                                                std::vector<int> haboviNajbolje,
                                                std::vector<std::vector<int> >& A, 
                                                std::vector<std::vector<int> >& B,
                                                double alfa,
                                                std::vector<std::vector<double> > cene,
                                                int brojCvorova,
                                                double POZITIVNA_BESKONACNOST);
void popuni_matrice_alokacije(std::vector<std::vector<double> >& dist,
                            std::vector<int> haboviNajbolje,
                            int p,
                            std::vector<std::vector<int> >& A,
                            std::vector<std::vector<int> >& B,
                            int brojCvorova,
                            double POZITIVNA_BESKONACNOST);