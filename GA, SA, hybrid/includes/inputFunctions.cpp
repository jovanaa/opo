#include "inputFunctions.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <chrono>

void ucitajCAB(const char* nazivFajla,
			int& brojCvorova,
			int& p,
			double& alfa,
			std::vector<std::vector<double> >& cene) {
	std::cout << "\nUnesite broj cvorova: ";
	std::cin >> brojCvorova;
	
	std::cout << "Unesite broj habova: ";
	std::cin >> p;

	std::cout << "Unesite vrednost za alfa: ";
	std::cin >> alfa;
		
	std::ifstream ulaz;
	ulaz.open(nazivFajla);
		
	cene.resize(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		cene[i].resize(brojCvorova);
		for (int j = 0; j < brojCvorova; j++) {
            // Ignore everything up to the first '.', or ulaz.ignore(15); would also work. See the CAB instances files. :)
			ulaz.ignore(std::numeric_limits<std::streamsize>::max(), '.');
			ulaz >> cene[i][j];
		}
	}
		
	ulaz.close();
}

void ucitajAP(const char* nazivFajla,
			int& brojCvorova,
			int& p,
			double& alfa,
			std::vector<std::vector<double> >& cene) {
	alfa = 0.75;

	std::ifstream ulaz;
	ulaz.open(nazivFajla);
		
	ulaz >> brojCvorova;
	
	std::vector<double> x, y;
	x.reserve(brojCvorova);
	y.reserve(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		ulaz >> x[i];
		ulaz >> y[i];
	}

	cene.resize(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		cene[i].resize(brojCvorova);
		for (int j = 0; j < brojCvorova; j++) {
			cene[i][j] = std::sqrt(std::pow(x[i] - x[j], 2) + std::pow(y[i] - y[j], 2));
		}
	}

	ulaz.ignore();	// Ignore new line character
	//Ignore next brojCvorova lines.
	for (int i = 0; i < brojCvorova; i++)
		ulaz.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	ulaz >> p;
		
	ulaz.close();
}

void ucitajURAND(const char* nazivFajla,
				int& brojCvorova,
				int& p,
				double& alfa,
				std::vector<std::vector<double> >& cene) {
	alfa = 0.75;

	std::ifstream ulaz;
	ulaz.open(nazivFajla);
		
	ulaz >> brojCvorova;
		
	std::vector<double> x, y;
	x.reserve(brojCvorova);
	y.reserve(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		ulaz >> x[i];
		ulaz >> y[i];
	}
	
	cene.resize(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		cene[i].resize(brojCvorova);
		for (int j = 0; j < brojCvorova; j++) {
			cene[i][j] = std::sqrt(std::pow(x[i] - x[j], 2) + std::pow(y[i] - y[j], 2));
		}
	}

	ulaz.ignore();	// Ignore new line character
	// Ignore next brojCvorova lines.
	for (int i = 0; i < brojCvorova; i++)
		ulaz.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	ulaz >> p;
		
	ulaz.close();
}