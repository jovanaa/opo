#include "allocationMatrices.h"
#include <iostream>
#include <algorithm>
#include <vector>

void inicijalizacija_pocetnih_matrica_alokacije (std::vector<std::vector<double> >& dist,
                                                std::vector<int> haboviNajbolje,
                                                std::vector<std::vector<int> >& A, 
                                                std::vector<std::vector<int> >& B,
                                                double alfa,
                                                std::vector<std::vector<double> > cene,
                                                int brojCvorova,
                                                double POZITIVNA_BESKONACNOST) {
    /*
    In this case gama = 1 and delta = 1. Constructing the initial dist matrix:
    dist[i][j] = infinity,              i, j are not hubs                       (neither)
    dist[i][j] = alfa * cene[i][j],     i and j are both hubs                   (both)
    dist[i][j] = cene[i][j],            i != j, exactly one of i, j is a hub    (exactly one)
    */
    /*
    If (gama != 1) and/or (delta != 1), constructing the initial dist matrix:
    dist[i][j] = infinity,                 i, j are not hubs                   (neither)
    dist[i][j] = alfa * cene[i][j],        i and j are both hubs               (both)
    dist[i][j] = delta * cene[i][j],       when i is a hub                     (only i is a hub)
    dist[i][j] = gama * cene[i][j],        when j is a hub                     (only j is a hub)
   */

    A.resize(brojCvorova);
    B.resize(brojCvorova);
	dist.resize(brojCvorova);
	for (int i = 0; i < brojCvorova; i++){
        A[i].resize(brojCvorova);
        B[i].resize(brojCvorova);
		dist[i].resize(brojCvorova);
		for (int j = 0; j < brojCvorova; j++){
            auto nadjiIuHabovi = std::find(std::begin(haboviNajbolje), std::end(haboviNajbolje), i);
            auto nadjiJuHabovi = std::find(std::begin(haboviNajbolje), std::end(haboviNajbolje), j);

            A[i][j] = POZITIVNA_BESKONACNOST;
            B[i][j] = POZITIVNA_BESKONACNOST;

            if (nadjiIuHabovi == std::end(haboviNajbolje) && nadjiJuHabovi == std::end(haboviNajbolje))
                dist[i][j] = POZITIVNA_BESKONACNOST;
            else if (nadjiIuHabovi != std::end(haboviNajbolje) && nadjiJuHabovi != std::end(haboviNajbolje))
                dist[i][j] = alfa * cene[i][j];
            else
                dist[i][j] = cene[i][j];

            #if 0
            // If (gama != 1) and/or (delta != 1), the last else statement should be REPLACED with:
            else if (nadjiIuHabovi != std::end(haboviNajbolje)))
                dist[i][j] = delta * cene[i][j];
            else
                dist[i][j] = gama * cene[i][j];
            #endif
		}
	}
}

// Modified Floyd - Warshall algorithm to find costs of shortest paths via hubs.
// Time complexity: O(p*n^2), where n = brojCvorova.
void popuni_matrice_alokacije (std::vector<std::vector<double> >& dist,
                            std::vector<int> haboviNajbolje,
                            int p,
                            std::vector<std::vector<int> >& A,
                            std::vector<std::vector<int> >& B,
                            int brojCvorova,
                            double POZITIVNA_BESKONACNOST) {

	for (int brojac = 0; brojac < p; brojac++) {
		for (int i = 0; i < brojCvorova; i++) {
			for (int j = 0; j < brojCvorova; j++) {
                auto nadjiIuHabovi = std::find(std::begin(haboviNajbolje), std::end(haboviNajbolje), i);
                auto nadjiJuHabovi = std::find(std::begin(haboviNajbolje), std::end(haboviNajbolje), j);

				if (dist[i][j] > dist[i][haboviNajbolje[brojac]] + dist[haboviNajbolje[brojac]][j]) {
					dist[i][j] = dist[i][haboviNajbolje[brojac]] + dist[haboviNajbolje[brojac]][j];

					if (A[i][haboviNajbolje[brojac]] != POZITIVNA_BESKONACNOST) {
						A[i][j] = A[i][haboviNajbolje[brojac]];
						B[i][j] = haboviNajbolje[brojac];
					} else if (A[haboviNajbolje[brojac]][j] != POZITIVNA_BESKONACNOST) {
						A[i][j] = haboviNajbolje[brojac];
						B[i][j] = A[haboviNajbolje[brojac]][j];
					} else {
						A[i][j] = haboviNajbolje[brojac];
						B[i][j] = POZITIVNA_BESKONACNOST;
					}
                }
            }
        }
    }
}