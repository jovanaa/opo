The "INSTANCES" directory contains instances by which parameters were set, and for which the implementations were tested.
The "INSTANCES-all" directory contains all AP, CAB and URAND instances.

Tag "ADJUST" marks parameters to adjust when using code on different types of instances. (see science paper)
RECOMMENDATION: To find parts that should be adjusted faster, add Tag "ADJUST" to Todo Tree.

ADJUST:
In GA.cpp:
- adjust parameter values
  Parameters: velicinaPopulacije,
		brojElitnihJedinki,
		velicinaTurnira,
		verovatnocaMutacije,
		MAX_VREME_GA
- choose which function ukrstanje() will be used
- adjust output file name

In SA.cpp:
- there are two options for simulirano_kaljenje() <- choose
- when using simulirano_kaljenje() which implements Algorithm 1, choose which value for "zavesa" will be used
- adjust parameter values
  Parameters: MAX_VREME_SA,
		pocetnaTemperatura,
		parametarHladjenja,
		maxPonavljanjaNaIstojTemp
The last three parameters should only be set when simulirano_kaljenje() which implements Algorithm 2 (with temperature) is used.
- adjust output file name

In "hybrid (GA + SA).cpp": Everything mentioned for SA and GA, except that instead of MAX_VREME_SA, MAX_ITERACIJA_SA is used.

NOTE: Parameters could have been set beforehand which would make running the programs easier. However, more efficiency is achieved by setting different parameter values for different types/sizes of instances.
----------------------------------------------------------------------------------------
COMPILE:
1) g++ -o SA SA.cpp includes/allocationMatrices.cpp includes/inputFunctions.cpp includes/outputFunctions.cpp includes/timer.cpp
2) g++ -o GA GA.cpp includes/allocationMatrices.cpp includes/inputFunctions.cpp includes/outputFunctions.cpp includes/timer.cpp
3) g++ -o hybrid "hybrid (GA + SA).cpp" includes/allocationMatrices.cpp includes/inputFunctions.cpp includes/outputFunctions.cpp includes/timer.cpp

RUN PROGRAMS:
1) ./SA Instances/<instance folder name>/<instance file name>
example: ./SA Instances/small/AP102.txt

2) ./GA Instances/<instance folder name>/<instance file name>
example: ./GA Instances/small/AP102.txt

3) ./hybrid Instances/<instance folder name>/<instance file name>
example: ./hybrid Instances/small/AP102.txt