#include <fstream>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <algorithm>
#include <iomanip>
#include <chrono>

#define POZITIVNA_BESKONACNOST 1e9
#define MAX_VREME_GA 1000

int brojCvorova;
std::vector<std::vector<double> > cene;
int p;
float alfa;
// int gama = 1, delta = 1;
double opt = POZITIVNA_BESKONACNOST;

struct Parametri {
	// GA
	// int MAX_ITERACIJA;
	int VELICINA_POPULACIJE;
	int ELITA;
	double VEROVATNOCA_MUTACIJE;
	int VELICINA_TURNIRA;
	int TIP_UKRSTANJA;
	// SA
	int MAX_ITERACIJA_SA;
	int tipZavese;
	// SA - Used only if tipZavese == 3
	double temperatura;
	double	parametarHladjenja;
	double	maxPonavljanjaNaIstojTemp;
};


class Stoperica {
public:
	Stoperica() {
		pocetniTrenutak = std::chrono::high_resolution_clock::now();
	}

	double izracunaj_proteklo_vreme() {
		auto krajnjiTrenutak = std::chrono::high_resolution_clock::now();

		auto pocetak = std::chrono::time_point_cast<std::chrono::microseconds>(pocetniTrenutak).time_since_epoch().count();
		auto kraj = std::chrono::time_point_cast<std::chrono::microseconds>(krajnjiTrenutak).time_since_epoch().count();

		auto trajanje = kraj - pocetak;			// microseconds
		double trajanje_ms = trajanje * 0.001;	// milliseconds

		return trajanje_ms;
	}

private:
	std::chrono::time_point<std::chrono::high_resolution_clock> pocetniTrenutak;
};


// TODO: Make a header file containing functions: ucitajCAB, ucitajAP and ucitajURAND.
void ucitajCAB(const std::string nazivFajla) {
	#if 0
	std::cout << "\nUnesite broj cvorova: ";
	std::cin >> brojCvorova;
	
	std::cout << "Unesite broj habova: ";
	std::cin >> p;

	std::cout << "Unesite vrednost za alfa: ";
	std::cin >> alfa;
	#endif
		
	std::ifstream ulaz;
	ulaz.open(nazivFajla);
		
	cene.resize(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		cene[i].resize(brojCvorova);
		for (int j = 0; j < brojCvorova; j++) {
            // Ignore everything up to the first '.', or ulaz.ignore(15); would also work. See the CAB instances files. :)
			ulaz.ignore(std::numeric_limits<std::streamsize>::max(), '.');
			ulaz >> cene[i][j];
		}
	}
		
	ulaz.close();
}

void ucitajAP(const std::string nazivFajla) {
	alfa = 0.75;

	std::ifstream ulaz;
	ulaz.open(nazivFajla);
		
	ulaz >> brojCvorova;
	
	std::vector<double> x, y;
	x.reserve(brojCvorova);
	y.reserve(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		ulaz >> x[i];
		ulaz >> y[i];
	}

	cene.resize(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		cene[i].resize(brojCvorova);
		for (int j = 0; j < brojCvorova; j++) {
			cene[i][j] = std::sqrt(std::pow(x[i] - x[j], 2) + std::pow(y[i] - y[j], 2));
		}
	}

	ulaz.ignore();	// Ignore new line character.
	//Ignore next brojCvorova lines.
	for (int i = 0; i < brojCvorova; i++)
		ulaz.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	ulaz >> p;
		
	ulaz.close();
}

void ucitajURAND(const std::string nazivFajla) {
	alfa = 0.75;

	std::ifstream ulaz;
	ulaz.open(nazivFajla);
		
	ulaz >> brojCvorova;
		
	std::vector<double> x, y;
	x.reserve(brojCvorova);
	y.reserve(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		ulaz >> x[i];
		ulaz >> y[i];
	}
	
	cene.resize(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		cene[i].resize(brojCvorova);
		for (int j = 0; j < brojCvorova; j++) {
			cene[i][j] = std::sqrt(std::pow(x[i] - x[j], 2) + std::pow(y[i] - y[j], 2));
		}
	}

	ulaz.ignore();	// Ignore new line character
	// Ignore next brojCvorova lines.
	for (int i = 0; i < brojCvorova; i++)
		ulaz.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	ulaz >> p;
		
	ulaz.close();
}

class Jedinka {
public:
	// Set of hubs. In the SA implementation this is a vector called "habovi".
	std::vector<int> kod;
	double fitnes;

	Jedinka() {
		kod.resize(p);
		for (int j = 0; j < p; j++) {
			kod[j] = std::rand() % brojCvorova;
		}
		
		fitnes = fitnes_funkcija();
	}
	
	// This function is the same as vrednost_resenja() in the SA implementation.
	double fitnes_funkcija() {
		std::vector<std::vector<double> > dist;

		inicijalizacija_pocetne_matrice(dist);
		pronadji_najkrace_puteve(dist);
		double max = pronadji_max_cenu_najkracih_puteva(dist);

		return max;
	}

private:
	// Time complexity: O(n^2*p), where n = brojCvorova
	void inicijalizacija_pocetne_matrice(std::vector<std::vector<double> >& dist) {
		/*
		In this case gama = 1 and delta = 1. Logic for constructing the initial dist matrix:
		dist[i][j] = infinity,              i, j are not hubs                       (neither)
		dist[i][j] = alfa * cene[i][j],     i and j are both hubs                   (both)
		dist[i][j] = cene[i][j],            i != j, exactly one of i, j is a hub    (exactly one)
		*/
		/*
		If (gama != 1) and/or (delta != 1), constructing the initial dist matrix:
		dist[i][j] = infinity,                 i, j are not hubs                   (neither)
		dist[i][j] = alfa * cene[i][j],        i and j are both hubs               (both)
		dist[i][j] = delta * cene[i][j],       when i is a hub                     (only i is a hub)
		dist[i][j] = gama * cene[i][j],        when j is a hub                     (only j is a hub)
		*/

		dist.resize(brojCvorova);
		for (int i = 0; i < brojCvorova; i++){
			dist[i].resize(brojCvorova);
			for (int j = 0; j < brojCvorova; j++){
				auto nadjiIuHabovi = std::find(std::begin(kod), std::end(kod), i);
				auto nadjiJuHabovi = std::find(std::begin(kod), std::end(kod), j);

				if (nadjiIuHabovi == std::end(kod) && nadjiJuHabovi == std::end(kod))
					dist[i][j] = POZITIVNA_BESKONACNOST;
				else if (nadjiIuHabovi != std::end(kod) && nadjiJuHabovi != std::end(kod))
					dist[i][j] = alfa * cene[i][j];
				else
					dist[i][j] = cene[i][j];

				/*
				If (gama != 1) and/or (delta != 1), the last else statement should be REPLACED with:
				else if (nadjiIuHabovi == std::end(kod))
					dist[i][j] = delta * cene[i][j];
				else
					dist[i][j] = gama * cene[i][j];
				*/

			}
		}
	}

	// Modified Floyd - Warshall algorithm to find costs of shortest paths via hubs
	// Time complexity: O(p*n^2), where n = brojCvorova
	void pronadji_najkrace_puteve(std::vector<std::vector<double> >& dist) {

		for (int brojac = 0; brojac < p; brojac++)
			for (int i = 0; i < brojCvorova; i++)
				for (int j = 0; j < brojCvorova; j++)
					// TODO: Could be replaced with: dist[i][j] = std::min(dist[i][j], dist[i][kod[brojac]] + dist[kod[brojac]][j]);	Check time impact.
					if (dist[i][j] > dist[i][kod[brojac]] + dist[kod[brojac]][j])
						dist[i][j] = dist[i][kod[brojac]] + dist[kod[brojac]][j];
	}

	// Time complexity: O(n^2), where n = brojCvorova
	double pronadji_max_cenu_najkracih_puteva(std::vector<std::vector<double> >& dist) {
		double max = 0;

		for (int i = 0; i < brojCvorova; i++)
			for (int j = 0; j < brojCvorova; j++)
				// TODO: Could be replaced with: max = std::max(max, dist[i][j]);	Check time impact.
				if (dist[i][j] > max)
					max = dist[i][j];

		return max;
	}
	
};

std::vector<Jedinka> populacija;
std::vector<Jedinka> novaPopulacija;
int iteracija = 0;

int selekcija(int velicinaTurnira,int velicinaPopulacije) {
	double min = POZITIVNA_BESKONACNOST;
	int pobednik = -1;
	
	for (int i = 0; i < velicinaTurnira; i++) {
		int k = std::rand() % velicinaPopulacije;
		
		if (populacija[k].fitnes < min) {
			min = populacija[k].fitnes;
			pobednik = k;
		}
	}
	
	return pobednik;
}

void jednopoziciono_ukrstanje(Jedinka& roditelj1, Jedinka& roditelj2, Jedinka& potomak1, Jedinka& potomak2) {
	int indeks = std::rand() % p;

	// TODO: Delete commented code.
	/*potomak1.kod.resize(0);
	potomak2.kod.resize(0);*/
	
	for (int i = 0; i < indeks; i++) {
		/*potomak1.kod.push_back(roditelj1.kod[i]);
		potomak2.kod.push_back(roditelj2.kod[i]);*/
		potomak1.kod[i] = roditelj1.kod[i];
		potomak2.kod[i] = roditelj2.kod[i];
	}
	
	for (int i = indeks; i < p; i++) {
		/*potomak1.kod.push_back(roditelj2.kod[i]);
		potomak2.kod.push_back(roditelj1.kod[i]);*/
		potomak1.kod[i] = roditelj1.kod[i];
		potomak2.kod[i] = roditelj2.kod[i];
	}

}

void dvopoziciono_ukrstanje(Jedinka& roditelj1, Jedinka& roditelj2, Jedinka& potomak1, Jedinka& potomak2) {
	// Pick two random indexes.
	int prviIndeks = std::rand() % p;
	int drugiIndeks;
	do {
		drugiIndeks = std::rand() % p;
	} while (prviIndeks == drugiIndeks);

	// Store the picked values so that: prviIndeks < drugiIndeks.
	if (drugiIndeks < prviIndeks) {
		int pomocna = prviIndeks;
		prviIndeks = drugiIndeks;
		drugiIndeks = pomocna;
	}
	
	/*potomak1.kod.resize(0);
	potomak2.kod.resize(0);*/
	
	for (int i = 0; i < prviIndeks; i++) {
		/*potomak1.kod.push_back(roditelj1.kod[i]);
		potomak2.kod.push_back(roditelj2.kod[i]);*/
		potomak1.kod[i] = roditelj1.kod[i];
		potomak2.kod[i] = roditelj2.kod[i];
	}
	
	for (int i = prviIndeks; i < drugiIndeks; i++) {
		/*potomak1.kod.push_back(roditelj2.kod[i]);
		potomak2.kod.push_back(roditelj1.kod[i]);*/
		potomak1.kod[i] = roditelj2.kod[i];
		potomak2.kod[i] = roditelj1.kod[i];
	}

	for (int i = drugiIndeks; i < p; i++) {
		/*potomak1.kod.push_back(roditelj1.kod[i]);
		potomak2.kod.push_back(roditelj2.kod[i]);*/
		potomak1.kod[i] = roditelj1.kod[i];
		potomak2.kod[i] = roditelj2.kod[i];
	}
}

void uniformno_ukrstanje(Jedinka& roditelj1, Jedinka& roditelj2, Jedinka& potomak1, Jedinka& potomak2) {
	/*potomak1.kod.resize(0);
	potomak2.kod.resize(0);*/
	
	for (int i = 0; i < p; i++) {
		int roditeljZaGenPotomka1 = std::rand() % 2;

		if (roditeljZaGenPotomka1 == 1) {
			/*potomak1.kod.push_back(roditelj1.kod[i]);
			potomak2.kod.push_back(roditelj2.kod[i]);*/
			potomak1.kod[i] = roditelj1.kod[i];
			potomak2.kod[i] = roditelj2.kod[i];
		} else {
			/*potomak1.kod.push_back(roditelj2.kod[i]);
			potomak2.kod.push_back(roditelj1.kod[i]);*/
			potomak1.kod[i] = roditelj2.kod[i];
			potomak2.kod[i] = roditelj1.kod[i];
		}
	}
}

void mutacija(Jedinka& jedinka, int verovatnocaMutacije) {
	int k;
	
	for (int i = 0; i < p; i++) {
		if (std::rand() % 100 > verovatnocaMutacije)
			continue;
			
		do {
			k = std::rand() % brojCvorova;
		} while (std::find(jedinka.kod.begin(), jedinka.kod.end(), k) != jedinka.kod.end());
		jedinka.kod[i] = k;
	}
}

bool uporedi(Jedinka i1, Jedinka i2) {
	return i1.fitnes < i2.fitnes;
}

// Choose element from the neighborhood of the current solution. Replace one hub with a non-hub node.
std::pair<int, int> invertuj(Jedinka& jedinka) {
	int k = std::rand() % p;
	int j = jedinka.kod[k];
	
	int i;
	do {
		i = std::rand() % brojCvorova;
	} while (std::find(jedinka.kod.begin(), jedinka.kod.end(), i) != jedinka.kod.end());
	jedinka.kod[k] = i;
	
	return std::make_pair(j, k);
}

void vrati(int j, int k, Jedinka& jedinka) {
	jedinka.kod[k] = j;
}

void algoritam1(Jedinka& jedinka, Parametri parametri, Stoperica& stopericaGA) {
	Jedinka najboljaJedinka = jedinka;
	double trenutnaVrednost = jedinka.fitnes;				// Current value: f(H)
	double najboljaVrednost = trenutnaVrednost;				// f* = f(H)
	int iteracija = 0;

	Stoperica stoperica;	// Start time.
	double protekloVremeSA = stoperica.izracunaj_proteklo_vreme();
	double protekloVremeGA = stopericaGA.izracunaj_proteklo_vreme();
	while (/*protekloVremeSA < MAX_VREME_SA &&*/ iteracija++ < parametri.MAX_ITERACIJA_SA && protekloVremeGA < MAX_VREME_GA) {
		std::pair<int, int> par = invertuj(jedinka);		// Choose random solution H' in the neighborhood of H.
		int j = par.first;
		int k = par.second;
		
		double novaVrednost = jedinka.fitnes_funkcija();	// New value: f(H')
		if (novaVrednost < trenutnaVrednost)				// if (f(H') < f(H))
			trenutnaVrednost = novaVrednost;				//	f(H) = f(H')
		else {
			double zavesa;
			if (parametri.tipZavese == 1)
				zavesa = log(2) / log(1 + iteracija);
			else if (parametri.tipZavese == 2)
				zavesa = 1.0 / std::pow(iteracija, 0.5);
			
			double q = ((double) rand() / (RAND_MAX));
			
			if (zavesa > q)
				trenutnaVrednost = novaVrednost;
			else
				vrati(j, k, jedinka);
		}
		
		if (novaVrednost < najboljaVrednost) {
			najboljaVrednost = novaVrednost;
			
			for (int i = 0; i < p; i++)
				najboljaJedinka.kod[i] = jedinka.kod[i];
			najboljaJedinka.fitnes = najboljaVrednost;
		}

		protekloVremeSA = stoperica.izracunaj_proteklo_vreme();
		protekloVremeGA = stopericaGA.izracunaj_proteklo_vreme();
	}

	jedinka = najboljaJedinka;
}

void algoritam2(Jedinka& jedinka, Parametri parametri, Stoperica& stopericaGA) {
	Jedinka najboljaJedinka = jedinka;
	double trenutnaVrednost = jedinka.fitnes;				// Current value: f(H)
	double najboljaVrednost = trenutnaVrednost;				// f* = f(H)

	Stoperica stoperica;	// Start time.
	double protekloVremeSA = stoperica.izracunaj_proteklo_vreme();
	double protekloVremeGA = stopericaGA.izracunaj_proteklo_vreme();
	while (parametri.temperatura > 0 /* min temperatura */ && protekloVremeGA < MAX_VREME_GA) {
		int brojPonavljanjaNaIstojTemp = 0;
		while (brojPonavljanjaNaIstojTemp < parametri.maxPonavljanjaNaIstojTemp && protekloVremeGA < MAX_VREME_GA) {
			std::pair<int, int> par = invertuj(jedinka);		// Choose random solution H' in the neighborhood of H.
			int j = par.first;
			int k = par.second;
		
			double novaVrednost = jedinka.fitnes_funkcija();	// New value: f(H')
			if (novaVrednost < trenutnaVrednost)				// if (f(H') < f(H))
				trenutnaVrednost = novaVrednost;				//	f(H) = f(H')
			else {
				double zavesa = exp((-1) * (novaVrednost - trenutnaVrednost) / parametri.temperatura);
				double q = ((double) rand() / (RAND_MAX));
				
				if (zavesa > q)
					trenutnaVrednost = novaVrednost;
				else
					vrati(j, k, jedinka);
			}
			
			if (novaVrednost < najboljaVrednost) {
				najboljaVrednost = novaVrednost;
				
				for (int i = 0; i < p; i++)
					najboljaJedinka.kod[i] = jedinka.kod[i];
				najboljaJedinka.fitnes = najboljaVrednost;
			}

			brojPonavljanjaNaIstojTemp++;
			protekloVremeSA = stoperica.izracunaj_proteklo_vreme();
			protekloVremeGA = stopericaGA.izracunaj_proteklo_vreme();
		}
		
		parametri.temperatura *= parametri.parametarHladjenja;
	}

	jedinka = najboljaJedinka;
}

void simulirano_kaljenje(Jedinka& jedinka, Parametri parametri, Stoperica& stopericaGA) {
	if (parametri.tipZavese == 3) {
		algoritam2(jedinka, parametri, stopericaGA);
	} else {	// parametri.tipZavese == 1 || parametri.tipZavese == 2
		algoritam1(jedinka, parametri, stopericaGA);
	}
}

Parametri ucitaj_parametre_za_GA(/* const char* vrMaxIter, */
								const char* vrVelicinePopulacije,
								const char* vrZaBrojElitnihJedinki,
								const char* vrVerovatnoceMutacije,
								const char* vrVelicineTurnira,
								const char* vrTipaUkrstanja) {
	Parametri parametri;

	// parametri.MAX_ITERACIJA = atoi(vrMaxIter);
	parametri.VELICINA_POPULACIJE = atoi(vrVelicinePopulacije);
	parametri.ELITA = atoi(vrZaBrojElitnihJedinki) % parametri.VELICINA_POPULACIJE;			// In case ELITA > VELICINA_POPULACIJE...see parameters file for irace.
	if (parametri.ELITA == 0)
		parametri.ELITA = parametri.VELICINA_POPULACIJE;
	parametri.VEROVATNOCA_MUTACIJE = atof(vrVerovatnoceMutacije);
	parametri.VELICINA_TURNIRA = atoi(vrVelicineTurnira) % parametri.VELICINA_POPULACIJE;	// In case VELICINA_TURNIRA > VELICINA_POPULACIJE...see parameters file for irace.
	if (parametri.VELICINA_TURNIRA == 0)
		parametri.VELICINA_TURNIRA = parametri.VELICINA_POPULACIJE;
	parametri.TIP_UKRSTANJA = atoi(vrTipaUkrstanja);

	return parametri;
}

Parametri ucitaj_parametre_za_SA(const char* vrMaxIter,
								const char* vrTipaZavese,
								const char* vrPocetneTemperature,
								const char* vrParametraHladjenja) {
	Parametri parametri;

	parametri.MAX_ITERACIJA_SA = atoi(vrMaxIter);
	parametri.tipZavese = atoi(vrTipaZavese);
	if (parametri.tipZavese == 3) {
		parametri.temperatura	= atof(vrPocetneTemperature);
		parametri.parametarHladjenja = atof(vrParametraHladjenja);	
		parametri.maxPonavljanjaNaIstojTemp = parametri.MAX_ITERACIJA_SA;	// in case tipZavese == 3, then MAX_ITERACIJA_SA will be maxPonavljanjaNaIstojTemp
	}
	
	return parametri;
}

int main(int argc, char** argv) {
	Parametri parametri;
	
	srand(atoi(argv[3]));

	// Will string work in ucitaj functions???
	std::string fajl = argv[4];
	// TODO: Change this!!! Don't harcode this :)
	std::string kojiJeSkupInst = fajl.substr(73, 2);
	//std::cout << fajl << "\n" << kojiJeSkupInst << std::endl;

	std::string cab = "CAB";
	std::string ap = "AP";
	std::string urand = "urand";

	// Without capping.
	#if 0
	// Using find because for kojiJeSkupInst I take 2 chars...in case of CAB I take CA.
	// Read instance file and parameters.
	if (cab.find(kojiJeSkupInst) != std::string::npos) {			// CAB instances
		brojCvorova = atoi(argv[6]);
		p = atoi(argv[8]);
		alfa = atof(argv[10]);	// NOTE TO SELF: alfa is a double...use atof() not atoi()
		ucitajCAB(fajl);

		parametri = ucitaj_parametre_za_GA(/*vargv[12] /* vrMaxIter , */
							argv[12] /* vrVelicinePopulacije */,
							argv[14] /* vrZaBrojElitnihJedinki */,
							argv[16] /* vrVerovatnoceMutacije */,
							argv[18] /* vrVelicineTurnira */,
							argv[20] /* vrTipaUkrstanja */);
		parametri = ucitaj_parametre_za_SA(argv[22] /* vrMaxIter */,
							argv[24] /* vrTipaZavese */,
							argv[26] /* vrPocetneTemperature */,
							argv[28] /* vrParametraHladjenja */);
	} else if (ap.find(kojiJeSkupInst) != std::string::npos) {		// AP instances
		ucitajAP(fajl);
		parametri = ucitaj_parametre_za_GA(/* argv[6] /* vrMaxIter */,
											argv[6] /* vrVelicinePopulacije */,
											argv[8] /* vrZaBrojElitnihJedinki */,
											argv[10] /* vrVerovatnoceMutacije */,
											argv[12] /* vrVelicineTurnira */,
											argv[14] /* vrTipaUkrstanja */);
		parametri = ucitaj_parametre_za_SA(argv[16] /* vrMaxIter */,
											argv[18] /* vrTipaZavese */,
											argv[20] /* vrPocetneTemperature */,
											argv[22] /* vrParametraHladjenja */); 
	} else if (urand.find(kojiJeSkupInst) != std::string::npos) {	// URAND instances
		ucitajURAND(fajl);
		parametri = ucitaj_parametre_za_GA(/* argv[6] /* vrMaxIter , */
											argv[6] /* vrTipaZavese */,
											argv[8] /* vrZaBrojElitnihJedinki */,
											argv[10] /* vrVerovatnoceMutacije */,
											argv[12] /* vrVelicineTurnira */,
											argv[14] /* vrTipaUkrstanja */);
		parametri = ucitaj_parametre_za_SA(argv[16] /* vrMaxIter */,
											argv[18] /* vrTipaZavese */,
											argv[20] /* vrPocetneTemperature */,
											argv[22] /* vrParametraHladjenja */);
	} else {
		std::cout << "Nekorektan unos.\n";
		return 0;
	}
	#endif

	// With capping.
	// TODO: Replace this with above code.
	// Using find because for kojiJeSkupInst I take 2 chars...in case of CAB I take CA.
	// Read instance file and parameters.
	if (cab.find(kojiJeSkupInst) != std::string::npos) {			// CAB instances
		brojCvorova = atoi(argv[6]);
		p = atoi(argv[8]);
		alfa = atof(argv[10]);	// NOTE TO SELF: alfa is a double...use atof() not atoi()
		ucitajCAB(fajl);

		parametri = ucitaj_parametre_za_GA(/* argv[13] /* vrMaxIter , */
							argv[13] /* vrVelicinePopulacije */,
							argv[15] /* vrZaBrojElitnihJedinki */,
							argv[17] /* vrVerovatnoceMutacije */,
							argv[19] /* vrVelicineTurnira */,
							argv[21] /* vrTipaUkrstanja */);
		parametri = ucitaj_parametre_za_SA(argv[23] /* vrMaxIter */,
							argv[25] /* vrTipaZavese */,
							argv[27] /* vrPocetneTemperature */,
							argv[29] /* vrParametraHladjenja */);
	} else if (ap.find(kojiJeSkupInst) != std::string::npos) {		// AP instances
		ucitajAP(fajl);
		parametri = ucitaj_parametre_za_GA(/* argv[7] /* vrMaxIter , */
											argv[7] /* vrVelicinePopulacije */,
											argv[9] /* vrZaBrojElitnihJedinki */,
											argv[11] /* vrVerovatnoceMutacije */,
											argv[13] /* vrVelicineTurnira */,
											argv[15] /* vrTipaUkrstanja */);
		parametri = ucitaj_parametre_za_SA(argv[17] /* vrMaxIter */,
											argv[19] /* vrTipaZavese */,
											argv[21] /* vrPocetneTemperature */,
											argv[23] /* vrParametraHladjenja */); 
	} else if (urand.find(kojiJeSkupInst) != std::string::npos) {	// URAND instances
		ucitajURAND(fajl);
		parametri = ucitaj_parametre_za_GA(/* argv[7] /* vrMaxIter , */
											argv[7] /* vrVelicinePopulacije */,
											argv[9] /* vrZaBrojElitnihJedinki */,
											argv[11] /* vrVerovatnoceMutacije */,
											argv[13] /* vrVelicineTurnira */,
											argv[15] /* vrTipaUkrstanja */);
		parametri = ucitaj_parametre_za_SA(argv[17] /* vrMaxIter */,
											argv[19] /* vrTipaZavese */,
											argv[21] /* vrPocetneTemperature */,
											argv[23] /* vrParametraHladjenja */);
	} else {
		std::cout << "Nekorektan unos.\n";
		return 0;
	}

	Stoperica stopericaGA;	// Start timer.

	populacija.reserve(parametri.VELICINA_POPULACIJE);
	novaPopulacija.reserve(parametri.VELICINA_POPULACIJE);
	for (int i = 0; i < parametri.VELICINA_POPULACIJE; i++) {
		populacija.emplace_back(Jedinka());
		novaPopulacija.emplace_back(Jedinka());
	}
	std::sort(populacija.begin(), populacija.end(), uporedi);
	simulirano_kaljenje(populacija[0], parametri, stopericaGA);
	
	double najboljaVrednost = populacija[0].fitnes;
	double protekloVremeGA = stopericaGA.izracunaj_proteklo_vreme();

	int neparanBrojJedinkiZaUkrstanjeIMutaciju = (parametri.VELICINA_POPULACIJE - parametri.ELITA) % 2;
	while (protekloVremeGA < MAX_VREME_GA /* && iteracija++ <  parametri.MAX_ITERACIJA */) {

		for (int i = 0; i < parametri.ELITA; i++)
			novaPopulacija[i] = populacija[i];
		
		// TODO: Change this when you set the ELITE parameter. Delete "neparanBrojJedinkiZaUkrstanjeIMutaciju" parameter.
		if (neparanBrojJedinkiZaUkrstanjeIMutaciju) {
			for (int i = parametri.ELITA; i < parametri.VELICINA_POPULACIJE-1; i+=2) {
				int i1 = selekcija(parametri.VELICINA_TURNIRA, parametri.VELICINA_POPULACIJE);
				int i2 = selekcija(parametri.VELICINA_TURNIRA, parametri.VELICINA_POPULACIJE);
				
				if (parametri.TIP_UKRSTANJA == 1)
					jednopoziciono_ukrstanje(populacija[i1], populacija[i2], novaPopulacija[i], novaPopulacija[i + 1]);
				else if (parametri.TIP_UKRSTANJA == 2)
					dvopoziciono_ukrstanje(populacija[i1], populacija[i2], novaPopulacija[i], novaPopulacija[i + 1]);
				else if (parametri.TIP_UKRSTANJA == 3)
					uniformno_ukrstanje(populacija[i1], populacija[i2], novaPopulacija[i], novaPopulacija[i + 1]);
				
				mutacija(novaPopulacija[i], parametri.VEROVATNOCA_MUTACIJE);
				mutacija(novaPopulacija[i + 1], parametri.VEROVATNOCA_MUTACIJE);
				
				novaPopulacija[i].fitnes = novaPopulacija[i].fitnes_funkcija();
				novaPopulacija[i + 1].fitnes = novaPopulacija[i + 1].fitnes_funkcija();
			}

			mutacija(novaPopulacija[parametri.VELICINA_POPULACIJE-1], parametri.VEROVATNOCA_MUTACIJE);
			novaPopulacija[parametri.VELICINA_POPULACIJE-1].fitnes = novaPopulacija[parametri.VELICINA_POPULACIJE-1].fitnes_funkcija();
		} else {
			for (int i = parametri.ELITA; i < parametri.VELICINA_POPULACIJE; i+=2) {
				int i1 = selekcija(parametri.VELICINA_TURNIRA, parametri.VELICINA_POPULACIJE);
				int i2 = selekcija(parametri.VELICINA_TURNIRA, parametri.VELICINA_POPULACIJE);
				
				if (parametri.TIP_UKRSTANJA == 1)
					jednopoziciono_ukrstanje(populacija[i1], populacija[i2], novaPopulacija[i], novaPopulacija[i + 1]);
				else if (parametri.TIP_UKRSTANJA == 2)
					dvopoziciono_ukrstanje(populacija[i1], populacija[i2], novaPopulacija[i], novaPopulacija[i + 1]);
				else if (parametri.TIP_UKRSTANJA == 3)
					uniformno_ukrstanje(populacija[i1], populacija[i2], novaPopulacija[i], novaPopulacija[i + 1]);
				
				mutacija(novaPopulacija[i], parametri.VEROVATNOCA_MUTACIJE);
				mutacija(novaPopulacija[i + 1], parametri.VEROVATNOCA_MUTACIJE);
				
				novaPopulacija[i].fitnes = novaPopulacija[i].fitnes_funkcija();
				novaPopulacija[i + 1].fitnes = novaPopulacija[i + 1].fitnes_funkcija();
			}
		}
		populacija = novaPopulacija;
		std::sort(populacija.begin(), populacija.end(), uporedi);

		simulirano_kaljenje(populacija[0], parametri, stopericaGA);
		
		protekloVremeGA = stopericaGA.izracunaj_proteklo_vreme();

		if (fajl ==  "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-hybrid/Instances/AP/AP102.txt" && std::abs(najboljaVrednost - 39922.11) < 0.009) {
			break;
		} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-hybrid/Instances/AP/AP103.txt" && std::abs(najboljaVrednost - 32713.94) < 0.009) {
			break;
		} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-hybrid/Instances/AP/AP104.txt" && std::abs(najboljaVrednost - 31577.96) < 0.009) {
			break;
		} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-hybrid/Instances/AP/AP105.txt" && std::abs(najboljaVrednost - 30371.32) < 0.009) {
			break;
		} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-hybrid/Instances/AP/AP202.txt" && std::abs(najboljaVrednost - 45954.15) < 0.009) {
			break;
		} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-hybrid/Instances/AP/AP203.txt" && std::abs(najboljaVrednost - 40909.59) < 0.009) {
			break;
		} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-hybrid/Instances/AP/AP204.txt" && std::abs(najboljaVrednost - 38320.25) < 0.009) {
			break;
		} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-hybrid/Instances/AP/AP205.txt" && std::abs(najboljaVrednost - 37868.15) < 0.009) {
			break;
		} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-hybrid/Instances/CAB/CAB10.txt" && alfa == 0.2 && std::abs(najboljaVrednost - 1421.88) < 0.009) {
			break;
		} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-hybrid/Instances/CAB/CAB10.txt" && alfa == 0.8 && std::abs(najboljaVrednost - 1749.04) < 0.009) {
			break;
		} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-hybrid/Instances/CAB/CAB10.txt" && alfa == 0.4 && std::abs(najboljaVrednost - 968.20) < 0.009) {
			break;
		} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-hybrid/Instances/CAB/CAB10.txt" && alfa == 0.6 && std::abs(najboljaVrednost - 1146.19) < 0.009) {
			break;
		} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-hybrid/Instances/CAB/CAB15.txt" && p == 2 && std::abs(najboljaVrednost - 2005.02) < 0.009) {
			break;
		} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-hybrid/Instances/CAB/CAB15.txt" && p == 3 && std::abs(najboljaVrednost - 1716.14) < 0.009) {
			break;
		} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-hybrid/Instances/CAB/CAB15.txt" && p == 4 && alfa == 0.2 && std::abs(najboljaVrednost - 1287.78) < 0.009) {
			break;
		} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-hybrid/Instances/CAB/CAB15.txt" && alfa == 1.0 && std::abs(najboljaVrednost - 2600.08) < 0.009) {
			break;
		} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-hybrid/Instances/CAB/CAB20.txt" && p == 3 && alfa == 0.6 && std::abs(najboljaVrednost - 1916.16) < 0.009) {
			break;
		} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-hybrid/Instances/CAB/CAB20.txt" && p == 3 && alfa == 1.0 && std::abs(najboljaVrednost - 2600.08) < 0.009) {
			break;
		} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-hybrid/Instances/CAB/CAB20.txt" && p == 4 && alfa == 0.6 && std::abs(najboljaVrednost - 1808.70) < 0.009) {
			break;
		} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-hybrid/Instances/CAB/CAB20.txt" && p == 4 && alfa == 1.0 && std::abs(najboljaVrednost - 2600.08) < 0.009) {
			break;
		}
	}
	
	/*
	std::sort(populacija.begin(), populacija.end(), uporedi);
	std::cout << populacija[0].fitnes << std::endl;
	*/
	
	std::cout << protekloVremeGA << " " << protekloVremeGA << std::endl;

	return 0;
}
