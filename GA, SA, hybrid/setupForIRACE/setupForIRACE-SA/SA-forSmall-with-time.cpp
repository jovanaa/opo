#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <algorithm>
#include <iomanip>
#include <chrono>

#define POZITIVNA_BESKONACNOST 1e9
#define MAX_VREME_SA 200

int brojCvorova;
std::vector<std::vector<double> > cene;
int p;
// Set of hubs. In the GA this is used as the code of an individual.
std::vector<int> habovi;
std::vector<int> haboviNajbolje;
double alfa;
// If gama and delta are not equal to 1, include appropriate declaration
// double gama = 1, delta = 1;

struct Parametri
{
	int MAX_ITERACIJA;
	int tipZavese;
	// Used only if tipZavese == 3
	double temperatura;
	double parametarHladjenja;
	double maxPonavljanjaNaIstojTemp;
};

class Stoperica {
public:
	Stoperica() {
		pocetniTrenutak = std::chrono::high_resolution_clock::now();
	}

	double izracunaj_proteklo_vreme() {
		auto krajnjiTrenutak = std::chrono::high_resolution_clock::now();

		auto pocetak = std::chrono::time_point_cast<std::chrono::microseconds>(pocetniTrenutak).time_since_epoch().count();
		auto kraj = std::chrono::time_point_cast<std::chrono::microseconds>(krajnjiTrenutak).time_since_epoch().count();

		auto trajanje = kraj - pocetak;			// microseconds
		double trajanje_ms = trajanje * 0.001;	// milliseconds

		return trajanje_ms;
	}

private:
	std::chrono::time_point<std::chrono::high_resolution_clock> pocetniTrenutak;
};

// TODO: Make a header file containing functions: ucitajCAB, ucitajAP and ucitajURAND.
void ucitajCAB(const std::string nazivFajla) {
	#if 0
	std::cout << "\nUnesite broj cvorova: ";
	std::cin >> brojCvorova;
	
	std::cout << "Unesite broj habova: ";
	std::cin >> p;

	std::cout << "Unesite vrednost za alfa: ";
	std::cin >> alfa;
	#endif
		
	std::ifstream ulaz;
	ulaz.open(nazivFajla);
		
	cene.resize(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		cene[i].resize(brojCvorova);
		for (int j = 0; j < brojCvorova; j++) {
            // Ignore everything up to the first '.', or ulaz.ignore(15); would also work. See the CAB instances files. :)
			ulaz.ignore(std::numeric_limits<std::streamsize>::max(), '.');
			ulaz >> cene[i][j];
		}
	}
		
	ulaz.close();
}

void ucitajAP(const std::string nazivFajla) {
	alfa = 0.75;

	std::ifstream ulaz;
	ulaz.open(nazivFajla);
		
	ulaz >> brojCvorova;
	
	std::vector<double> x, y;
	x.reserve(brojCvorova);
	y.reserve(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		ulaz >> x[i];
		ulaz >> y[i];
	}

	cene.resize(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		cene[i].resize(brojCvorova);
		for (int j = 0; j < brojCvorova; j++) {
			cene[i][j] = std::sqrt(std::pow(x[i] - x[j], 2) + std::pow(y[i] - y[j], 2));
		}
	}

	ulaz.ignore();	// Ignore new line character.
	//Ignore next brojCvorova lines.
	for (int i = 0; i < brojCvorova; i++)
		ulaz.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	ulaz >> p;
		
	ulaz.close();
}

void ucitajURAND(const std::string nazivFajla) {
	alfa = 0.75;

	std::ifstream ulaz;
	ulaz.open(nazivFajla);
		
	ulaz >> brojCvorova;
		
	std::vector<double> x, y;
	x.reserve(brojCvorova);
	y.reserve(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		ulaz >> x[i];
		ulaz >> y[i];
	}
	
	cene.resize(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		cene[i].resize(brojCvorova);
		for (int j = 0; j < brojCvorova; j++) {
			cene[i][j] = std::sqrt(std::pow(x[i] - x[j], 2) + std::pow(y[i] - y[j], 2));
		}
	}

	ulaz.ignore();	// Ignore new line character
	// Ignore next brojCvorova lines.
	for (int i = 0; i < brojCvorova; i++)
		ulaz.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	ulaz >> p;
		
	ulaz.close();
}

void inicijalizuj() {
	std::vector<double> maxTrosakZaDatiCvor;
	maxTrosakZaDatiCvor.resize(brojCvorova);
	habovi.resize(p);
	std::vector<std::pair<double, int> > parIndeksVr;
	
	// For each node h find maximum cost of transport, considering all costs form h to every other node.
	// That is, find max(chj), where j goes through the set of nodes.
	for (int i = 0; i < brojCvorova; i++) {
			maxTrosakZaDatiCvor[i] = cene[i][0];
		for (int j = 1; j < brojCvorova; j++) {
			if (maxTrosakZaDatiCvor[i] < cene[i][j])
				maxTrosakZaDatiCvor[i] = cene[i][j];
		}
		
		parIndeksVr.push_back(std::make_pair(maxTrosakZaDatiCvor[i], i));
	}
	
	// Sort the array of the maximum costs in ascending order. Keep track of indexes of nodes.
	std::sort(parIndeksVr.begin(), parIndeksVr.end());
	
	// Take p nodes with the minimal previously found maximal costs. Set those p nodes to be hubs.
	for (int i = 0; i < p; i++) {
		habovi[i] = parIndeksVr[i].second;
		//std::cout << habovi[i] << "\n";
	}
}

// Time complexity: O(n^2*p), where n = brojCvorova.
void inicijalizacija_pocetne_matrice(std::vector<std::vector<double> >& dist) {
    /*
    In this case gama = 1 and delta = 1. Constructing the initial dist matrix:
    dist[i][j] = infinity,              i, j are not hubs                       (neither)
    dist[i][j] = alfa * cene[i][j],     i and j are both hubs                   (both)
    dist[i][j] = cene[i][j],            i != j, exactly one of i, j is a hub    (exactly one)
    */
    /*
    If (gama != 1) and/or (delta != 1), constructing the initial dist matrix:
    dist[i][j] = infinity,                 i, j are not hubs                   (neither)
    dist[i][j] = alfa * cene[i][j],        i and j are both hubs               (both)
    dist[i][j] = delta * cene[i][j],       when i is a hub                     (only i is a hub)
    dist[i][j] = gama * cene[i][j],        when j is a hub                     (only j is a hub)
   */

	dist.resize(brojCvorova);
	for (int i = 0; i < brojCvorova; i++){
		dist[i].resize(brojCvorova);
		for (int j = 0; j < brojCvorova; j++){
            auto nadjiIuHabovi = std::find(std::begin(habovi), std::end(habovi), i);
            auto nadjiJuHabovi = std::find(std::begin(habovi), std::end(habovi), j);

            if (nadjiIuHabovi == std::end(habovi) && nadjiJuHabovi == std::end(habovi))
                dist[i][j] = POZITIVNA_BESKONACNOST;
            else if (nadjiIuHabovi != std::end(habovi) && nadjiJuHabovi != std::end(habovi))
                dist[i][j] = alfa * cene[i][j];
            else
                dist[i][j] = cene[i][j];

            /*
            If (gama != 1) and/or (delta != 1), the last else statement should be REPLACED with:
            else if (nadjiIuHabovi == std::end(habovi))
                dist[i][j] = delta * cene[i][j];
            else
                dist[i][j] = gama * cene[i][j];
            */

		}
	}
}

// Modified Floyd - Warshall algorithm to find costs of shortest paths via hubs.
// Time complexity: O(p*n^2), where n = brojCvorova
void pronadji_najkrace_puteve(std::vector<std::vector<double> >& dist) {

	for (int brojac = 0; brojac < p; brojac++)
		for (int i = 0; i < brojCvorova; i++)
			for (int j = 0; j < brojCvorova; j++)
                // TODO: Could be replaced with: dist[i][j] = std::min(dist[i][j], dist[i][kod[brojac]] + dist[kod[brojac]][j]);	Check time impact.
				if (dist[i][j] > dist[i][habovi[brojac]] + dist[habovi[brojac]][j])
					dist[i][j] = dist[i][habovi[brojac]] + dist[habovi[brojac]][j];
}

// Time complexity: O(n^2), where n = brojCvorova.
double pronadji_max_cenu_najkracih_puteva(std::vector<std::vector<double> >& dist) {
    double max = 0;

	for (int i = 0; i < brojCvorova; i++)
		for (int j = 0; j < brojCvorova; j++)
            // TODO: Could be replaced with: max = std::max(max, dist[i][j]);	Check time impact.
            if (dist[i][j] > max)
                max = dist[i][j];

	return max;
}

double vrednost_resenja() {
	std::vector<std::vector<double> > dist;

	inicijalizacija_pocetne_matrice(dist);
	pronadji_najkrace_puteve(dist);
	double max = pronadji_max_cenu_najkracih_puteva(dist);

	return max;
}


// Choose element from the neighborhood of the current solution. Replace one hub with a non-hub node.
std::pair<int, int> invertuj() {
	int k = std::rand() % p;
	int j = habovi[k];
	
	int i;
	do {
		i = std::rand() % brojCvorova;
	} while (std::find(habovi.begin(), habovi.end(), i) != habovi.end());
	habovi[k] = i;
	
	return std::make_pair(j, k);
}

void vrati(int j, int k) {
	habovi[k] = j;
}

double dostignut_optimum(std::string fajl, double najboljaVrednost) {
	int dostignutOptimum = 0;

	if (fajl ==  "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-SA/Instances/AP/AP102.txt" && std::abs(najboljaVrednost - 39922.11) < 0.009) {
		dostignutOptimum = 1;
	} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-SA/Instances/AP/AP103.txt" && std::abs(najboljaVrednost - 32713.94) < 0.009) {
		dostignutOptimum = 1;
	} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-SA/Instances/AP/AP104.txt" && std::abs(najboljaVrednost - 31577.96) < 0.009) {
		dostignutOptimum = 1;
	} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-SA/Instances/AP/AP105.txt" && std::abs(najboljaVrednost - 30371.32) < 0.009) {
		dostignutOptimum = 1;
	} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-SA/Instances/AP/AP202.txt" && std::abs(najboljaVrednost - 45954.15) < 0.009) {
		dostignutOptimum = 1;
	} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-SA/Instances/AP/AP203.txt" && std::abs(najboljaVrednost - 40909.59) < 0.009) {
		dostignutOptimum = 1;
	} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-SA/Instances/AP/AP204.txt" && std::abs(najboljaVrednost - 38320.25) < 0.009) {
		dostignutOptimum = 1;
	} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-SA/Instances/AP/AP205.txt" && std::abs(najboljaVrednost - 37868.15) < 0.009) {
		dostignutOptimum = 1;
	} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-SA/Instances/CAB/CAB10.txt" && alfa == 0.2 && std::abs(najboljaVrednost - 1421.88) < 0.009) {
		dostignutOptimum = 1;
	} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-SA/Instances/CAB/CAB10.txt" && alfa == 0.8 && std::abs(najboljaVrednost - 1749.04) < 0.009) {
		dostignutOptimum = 1;
	} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-SA/Instances/CAB/CAB10.txt" && alfa == 0.4 && std::abs(najboljaVrednost - 968.20) < 0.009) {
		dostignutOptimum = 1;
	} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-SA/Instances/CAB/CAB10.txt" && alfa == 0.6 && std::abs(najboljaVrednost - 1146.19) < 0.009) {
		dostignutOptimum = 1;
	} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-SA/Instances/CAB/CAB15.txt" && p == 2 && std::abs(najboljaVrednost - 2005.02) < 0.009) {
		dostignutOptimum = 1;
	} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-SA/Instances/CAB/CAB15.txt" && p == 3 && std::abs(najboljaVrednost - 1716.14) < 0.009) {
		dostignutOptimum = 1;
	} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-SA/Instances/CAB/CAB15.txt" && p == 4 && alfa == 0.2 && std::abs(najboljaVrednost - 1287.78) < 0.009) {
		dostignutOptimum = 1;
	} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-SA/Instances/CAB/CAB15.txt" && alfa == 1.0 && std::abs(najboljaVrednost - 2600.08) < 0.009) {
		dostignutOptimum = 1;
	} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-SA/Instances/CAB/CAB20.txt" && p == 3 && alfa == 0.6 && std::abs(najboljaVrednost - 1916.16) < 0.009) {
		dostignutOptimum = 1;
	} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-SA/Instances/CAB/CAB20.txt" && p == 3 && alfa == 1.0 && std::abs(najboljaVrednost - 2600.08) < 0.009) {
		dostignutOptimum = 1;
	} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-SA/Instances/CAB/CAB20.txt" && p == 4 && alfa == 0.6 && std::abs(najboljaVrednost - 1808.70) < 0.009) {
		dostignutOptimum = 1;
	} else if (fajl == "C:/Users/DeveloperK/Desktop/setupForIRACE/setupForIRACE-SA/Instances/CAB/CAB20.txt" && p == 4 && alfa == 1.0 && std::abs(najboljaVrednost - 2600.08) < 0.009) {
		dostignutOptimum = 1;
	}

	return dostignutOptimum;
}

double algoritam1(Parametri parametri, std::string fajl) {
	double trenutnaVrednost = vrednost_resenja();	// Current value: f(H)
	double najboljaVrednost = trenutnaVrednost;		// f* = f(H)
	int iteracija = 0;
	
	haboviNajbolje.resize(p); // TODO: Make this better. Maybe not global but return with this function, using a struct.

	Stoperica stoperica;	// Start time.
	double protekloVremeSA = stoperica.izracunaj_proteklo_vreme();
	while (/*protekloVremeSA < MAX_VREME_SA &&*/ iteracija++ < parametri.MAX_ITERACIJA) {
		std::pair<int, int> par = invertuj();		// Choose random solution H' in the neighborhood of H.
		int j = par.first;
		int k = par.second;
		
		double novaVrednost = vrednost_resenja();	// New value: f(H')
		if (novaVrednost < trenutnaVrednost)		// if (f(H') < f(H))
			trenutnaVrednost = novaVrednost;		// f(H) = f(H')
		else {
			double zavesa;
			if (parametri.tipZavese == 1)
				zavesa = log(2) / log(1 + iteracija);
			else if (parametri.tipZavese == 2)
				zavesa = 1.0 / std::pow(iteracija, 0.5);

			double q = ((double) rand() / (RAND_MAX));
				
			if (zavesa > q)
				trenutnaVrednost = novaVrednost;
			else 
				vrati(j, k);
				
		}
			
		if (novaVrednost < najboljaVrednost) {
			najboljaVrednost = novaVrednost;
			haboviNajbolje = habovi;			// NOTE TO SELF: Hubs may change because we allow worse solutions. Save the best combo.
		}

		protekloVremeSA = stoperica.izracunaj_proteklo_vreme();

		int dostignutOptimum = dostignut_optimum(fajl, najboljaVrednost);
		if (dostignutOptimum)
			return protekloVremeSA;
	}

	return POZITIVNA_BESKONACNOST;
}

double algoritam2(Parametri parametri, std::string fajl) {
	double trenutnaVrednost = vrednost_resenja();	// Current value: f(H)
	double najboljaVrednost = trenutnaVrednost;		// f* = f(H)
	
	haboviNajbolje.resize(p); // TODO: Make this better. Maybe not global but return with this function, using a struct.

	Stoperica stoperica;	// Start time.
	double protekloVremeSA = stoperica.izracunaj_proteklo_vreme();
	while (parametri.temperatura > 0 /* min temperatura */) {
		int brojPonavljanjaNaIstojTemp = 0;
		while (brojPonavljanjaNaIstojTemp < parametri.maxPonavljanjaNaIstojTemp) {
			std::pair<int, int> par = invertuj();		// Choose random solution H' in the neighborhood of H.
			int j = par.first;
			int k = par.second;
			
			double novaVrednost = vrednost_resenja();	// New value: f(H')
			if (novaVrednost < trenutnaVrednost)		// if (f(H') < f(H))
				trenutnaVrednost = novaVrednost;		// f(H) = f(H')
			else {
				double zavesa = exp((-1) * (novaVrednost - trenutnaVrednost) / parametri.temperatura);
				double q = ((double) rand() / (RAND_MAX));
				
				if (zavesa > q)
					trenutnaVrednost = novaVrednost;
				else 
					vrati(j, k);
				
			}
			
			if (novaVrednost < najboljaVrednost) {
				najboljaVrednost = novaVrednost;
				haboviNajbolje = habovi;			// NOTE TO SELF: Hubs may change because we allow worse solutions. Save the best combo.
			}
			
			brojPonavljanjaNaIstojTemp++;
			protekloVremeSA = stoperica.izracunaj_proteklo_vreme();

			int dostignutOptimum = dostignut_optimum(fajl, najboljaVrednost);
			if (dostignutOptimum)
				return protekloVremeSA;
		}
		
		parametri.temperatura *= parametri.parametarHladjenja;
	}

	return POZITIVNA_BESKONACNOST;
}

/*
// This variable will be asigned a value only if tipZavese == 3
double temperatura, parametarHladjenja;
int maxPonavljanjaNaIstojTemp;
*/
double simulirano_kaljenje(Parametri parametri, std::string fajl) {
	double resenje;
	if (parametri.tipZavese == 3) {
		resenje = algoritam2(parametri, fajl);
	} else {	// parametri.tipZavese == 1 || parametri.tipZavese == 2
		resenje = algoritam1(parametri, fajl);
	}
	
	return resenje;
}

Parametri ucitaj_parametre_za_SA(const char* vrMaxIter,
								const char* vrTipaZavese,
								const char* vrPocetneTemperature,
								const char* vrParametraHladjenja) {
	Parametri parametri;

	parametri.MAX_ITERACIJA = atoi(vrMaxIter);
	parametri.tipZavese = atoi(vrTipaZavese);
	if (parametri.tipZavese == 3) {
		parametri.temperatura	= atof(vrPocetneTemperature);
		parametri.parametarHladjenja = atof(vrParametraHladjenja);	
		parametri.maxPonavljanjaNaIstojTemp = parametri.MAX_ITERACIJA;
	}
	
	return parametri;
}

int main(int argc, char** argv) {
	Parametri parametri;
	
	srand(atoi(argv[3]));

	// Will string work in ucitaj functions???
	std::string fajl = argv[4];
	// TODO: Change this!!!
	std::string kojiJeSkupInst = fajl.substr(69, 2);
	//std::cout << fajl << "\n" << kojiJeSkupInst << std::endl;
	
	std::string cab = "CAB";
	std::string ap = "AP";
	std::string urand = "urand";

	// Using find because for kojiJeSkupInst I take 2 chars...in case of CAB I take CA.
	// Read instance file and parameters.
	if (cab.find(kojiJeSkupInst) != std::string::npos) {			// CAB instances
		brojCvorova = atoi(argv[6]);
		p = atoi(argv[8]);
		alfa = atof(argv[10]);	// NOTE TO SELF: alfa is a double...use atof() not atoi()
		ucitajCAB(fajl);

		parametri = ucitaj_parametre_za_SA(argv[12] /* vrMaxIter */,
							argv[14] /* vrTipaZavese */,
							argv[16] /* vrPocetneTemperature */,
							argv[18] /* vrParametraHladjenja */);
	} else if (ap.find(kojiJeSkupInst) != std::string::npos) {		// AP instances
		ucitajAP(fajl);
		parametri = ucitaj_parametre_za_SA(argv[6] /* vrMaxIter */,
											argv[8] /* vrTipaZavese */,
											argv[10] /* vrPocetneTemperature */,
											argv[12] /* vrParametraHladjenja */); 
	} else if (urand.find(kojiJeSkupInst) != std::string::npos) {	// URAND instances
		ucitajURAND(fajl);
		parametri = ucitaj_parametre_za_SA(argv[6] /* vrMaxIter */,
											argv[8] /* vrTipaZavese */,
											argv[10] /* vrPocetneTemperature */,
											argv[12] /* vrParametraHladjenja */);
	} else {
		std::cout << "Nekorektan unos.\n";
		return 0;
	}

#if  0
	if (cab.find(kojiJeSkupInst) != std::string::npos) {			// CAB instances
		brojCvorova = atoi(argv[6]);
		p = atoi(argv[8]);
		alfa = atof(argv[10]);	// NOTE TO SELF: alfa is a double...use atof() not atoi()
		ucitajCAB(fajl);

		parametri = ucitaj_parametre_za_SA(argv[13] /* vrMaxIter */,
							argv[15] /* vrTipaZavese */,
							argv[17] /* vrPocetneTemperature */,
							argv[19] /* vrParametraHladjenja */);
	} else if (ap.find(kojiJeSkupInst) != std::string::npos) {		// AP instances
		ucitajAP(fajl);
		parametri = ucitaj_parametre_za_SA(argv[7] /* vrMaxIter */,
											argv[9] /* vrTipaZavese */,
											argv[11] /* vrPocetneTemperature */,
											argv[13] /* vrParametraHladjenja */; 
	} else if (urand.find(kojiJeSkupInst) != std::string::npos) {	// URAND instances
		ucitajURAND(fajl);
		parametri = ucitaj_parametre_za_SA(argv[7] /* vrMaxIter */,
											argv[9] /* vrTipaZavese */,
											argv[11] /* vrPocetneTemperature */,
											argv[13] /* vrParametraHladjenja */);
	} else {
		std::cout << "Nekorektan unos.\n";
		return 0;
	}
#endif

	inicijalizuj();
	// double vrednostResenja = simulirano_kaljenje(parametri);
	double vremeDoDostizanjaOptResenja = simulirano_kaljenje(parametri, fajl);
	std::cout << vremeDoDostizanjaOptResenja << " " << vremeDoDostizanjaOptResenja;

	return 0;
}