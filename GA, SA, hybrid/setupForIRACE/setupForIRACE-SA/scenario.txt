###################################################### -*- mode: r -*- #####
## Scenario setup for Iterated Race (irace).
############################################################################

## To use the default value of a parameter of iRace, simply do not set
## the parameter (comment it out in this file, and do not give any
## value on the command line).

debugLevel = 3
digits = 2
#recoveryFile = "./irace-large-4.Rdata"

## File that contains the description of the parameters of the target
## algorithm.
parameterFile = "./parameters.txt"

## Directory where the programs will be run.
execDir = "./"

## File to save tuning results as an R dataset, either absolute path or
## relative to execDir.
logFile = "./irace.Rdata"

## Directory where training instances are located; either absolute path or
## relative to current directory. If no trainInstancesFiles is provided,
## all the files in trainInstancesDir will be listed as instances.
trainInstancesDir = "./Instances"

## File that contains a list of training instances and optionally
## additional parameters for them. If trainInstancesDir is provided, irace
## will search for the files in this folder.
trainInstancesFile = "./instances-list.txt"

## Script called for each configuration that executes the target algorithm
## to be tuned. See templates.
targetRunner = "./SA-with-time-but-minimisingCost.exe"

## Maximum number of runs (invocations of targetRunner) that will be
## performed. It determines the maximum budget of experiments for the
## tuning.
maxExperiments = 200
#maxTime = 14400

## Maximum total execution time in seconds for the executions of
## targetRunner. targetRunner must return two values: cost and time.
# maxTime = 0

## Debug level of the output of irace. Set this to 0 to silence all debug
## messages. Higher values provide more verbose debug messages.
# debugLevel = 0

## Statistical test used for elimination. Default test is always F-test
## unless capping is enabled, in which case the default test is t-test.
## Valid values are: F-test (Friedman test), t-test (pairwise t-tests with
## no correction), t-test-bonferroni (t-test with Bonferroni's correction
## for multiple comparisons), t-test-holm (t-test with Holm's correction
## for multiple comparisons).
# testType = "F-test"

## Seed of the random number generator (by default, generate a random
## seed).
# seed = NA

## Directory where testing instances are located, either absolute or
## relative to current directory.
testInstancesDir = "./Instances"

## File containing a list of test instances and optionally additional
## parameters for them.
testInstancesFile = "./instances-list.txt"

##############################################################################

## Enable the use of adaptive capping, a technique designed for minimizing
## the computation time of configurations. This is only available when
## elitist is active.
capping = 0

## Measure used to obtain the execution bound from the performance of the
## elite configurations: median, mean, worst, best.
cappingType = "median"
## Method to calculate the mean performance of elite configurations:
## candidate or instance.
boundType = "instance"

## Maximum execution bound for targetRunner. It must be specified when
## capping is enabled.
#boundMax = 100000

## Precision used for calculating the execution time. It must be specified
## when capping is enabled.
# boundDigits = 0

## Penalization constant for timed out executions (executions that reach
## boundMax execution time).
# boundPar = 1

## Replace the configuration cost of bounded executions with boundMax.
# boundAsTimeout = 1

## END of scenario file
############################################################################
#testType = "t-test"

