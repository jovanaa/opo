This code has not been refactored.

In SA algorithms parameter MAX_ITERACIJA is used as stopping criteria when tipZavese in {1, 2}.
When tipZavese == 3, MAX_ITERACIJA is used as the maximum number of iterations on constant temperature.
(These could have been two separate parameters)

The same goes for MAX_ITERACIJA_SA in hybrid algorithms.