#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <algorithm>
#include "../includes/typesOfInstances.h"
#include "../includes/inputFunctions.h"
#include "../includes/timer.h"
#include "../includes/allocationMatrices.h"
#include "../includes/outputFunctions.h"

constexpr int  POZITIVNA_BESKONACNOST = 1e9;

// GA.
constexpr int VELICINA_POPULACIJE = 59;
constexpr int  ELITA = 26;
constexpr int VELICINA_TURNIRA = 6;
constexpr double  VEROVATNOCA_MUTACIJE = 0.6803;
double  MAX_VREME_GA;
// SA.
constexpr int MAX_ITERACIJA_SA = 2935;
#if 0	// Use in case Algorithm 2 is being used for SA. Adjust parameter values.
constexpr int maxPonavljanjaNaIstojTemp = 4097;
constexpr double pocetnaTemperatura = 43482.6461;
constexpr double parametarHladjenja = 0.4709;
#endif


int brojCvorova;
std::vector<std::vector<double> > cene;
int p;
double alfa;
// double gama = 1, delta = 1;

void ucitajCAB(const char* nazivFajla, std::ifstream& fajlSaTestInstancama) {
	/*std::cout << "\nUnesite broj cvorova: ";
	std::cin >> brojCvorova;
	
	std::cout << "Unesite broj habova: ";
	std::cin >> p;

	std::cout << "Unesite vrednost za alfa: ";
	std::cin >> alfa;*/

	fajlSaTestInstancama >> brojCvorova;
	fajlSaTestInstancama >> p;
	fajlSaTestInstancama >> alfa;
		
	std::ifstream ulaz;
	ulaz.open(nazivFajla);
	
	cene.resize(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		cene[i].resize(brojCvorova);
		for (int j = 0; j < brojCvorova; j++) {
            // Ignore everything up to the first '.', or ulaz.ignore(15); would also work. See the CAB instances files. :)
			ulaz.ignore(std::numeric_limits<std::streamsize>::max(), '.');
			ulaz >> cene[i][j];
		}
	}
		
	ulaz.close();
}

void ucitajAP(const char* nazivFajla) {
	alfa = 0.75;

	std::ifstream ulaz;
	ulaz.open(nazivFajla);
		
	ulaz >> brojCvorova;

	std::vector<double> x, y;
	x.reserve(brojCvorova);
	y.reserve(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		ulaz >> x[i];
		ulaz >> y[i];
	}

	cene.resize(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		cene[i].resize(brojCvorova);
		for (int j = 0; j < brojCvorova; j++) {
			cene[i][j] = std::sqrt(std::pow(x[i] - x[j], 2) + std::pow(y[i] - y[j], 2));
		}
	}

	ulaz.ignore();	// Ignore new line character
	//Ignore next brojCvorova lines.
	for (int i = 0; i < brojCvorova; i++)
		ulaz.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	ulaz >> p;
		
	ulaz.close();
}

void ucitajURAND(const char* nazivFajla) {
	alfa = 0.75;

	std::ifstream ulaz;
	ulaz.open(nazivFajla);
		
	ulaz >> brojCvorova;
		
	std::vector<double> x, y;
	x.reserve(brojCvorova);
	y.reserve(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		ulaz >> x[i];
		ulaz >> y[i];
	}
	
	cene.resize(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		cene[i].resize(brojCvorova);
		for (int j = 0; j < brojCvorova; j++) {
			cene[i][j] = std::sqrt(std::pow(x[i] - x[j], 2) + std::pow(y[i] - y[j], 2));
		}
	}

	ulaz.ignore();	// Ignore new line character
	// Ignore next brojCvorova lines.
	for (int i = 0; i < brojCvorova; i++)
		ulaz.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	ulaz >> p;
		
	ulaz.close();
}

class Jedinka {
public:
	// Set of hubs. In the SA implementation this is a vector called "habovi".
	std::vector<int> kod;
	double fitnes;

	Jedinka() {
		kod.resize(p);
		for (int j = 0; j < p; j++) {
			kod[j] = std::rand() % brojCvorova;
		}
		
		fitnes = fitnes_funkcija();
	}
	
	// This function is the same as vrednost_resenja() in the SA implementation.
	double fitnes_funkcija() {
		std::vector<std::vector<double> > dist;

		inicijalizacija_pocetne_matrice(dist);
		pronadji_najkrace_puteve(dist);
		double max = pronadji_max_cenu_najkracih_puteva(dist);

		return max;
	}

private:
	// Time complexity: O(n^2*p), where n = brojCvorova
	void inicijalizacija_pocetne_matrice(std::vector<std::vector<double> >& dist) {
		/*
		In this case gama = 1 and delta = 1. Logic for constructing the initial dist matrix:
		dist[i][j] = infinity,              i, j are not hubs                       (neither)
		dist[i][j] = alfa * cene[i][j],     i and j are both hubs                   (both)
		dist[i][j] = cene[i][j],            i != j, exactly one of i, j is a hub    (exactly one)
		*/
		/*
		If (gama != 1) and/or (delta != 1), constructing the initial dist matrix:
		dist[i][j] = infinity,                 i, j are not hubs                   (neither)
		dist[i][j] = alfa * cene[i][j],        i and j are both hubs               (both)
		dist[i][j] = delta * cene[i][j],       when i is a hub                     (only i is a hub)
		dist[i][j] = gama * cene[i][j],        when j is a hub                     (only j is a hub)
		*/

		dist.resize(brojCvorova);
		for (int i = 0; i < brojCvorova; i++){
			dist[i].resize(brojCvorova);
			for (int j = 0; j < brojCvorova; j++){
				auto nadjiIuHabovi = std::find(std::begin(kod), std::end(kod), i);
				auto nadjiJuHabovi = std::find(std::begin(kod), std::end(kod), j);

				if (nadjiIuHabovi == std::end(kod) && nadjiJuHabovi == std::end(kod))
					dist[i][j] = POZITIVNA_BESKONACNOST;
				else if (nadjiIuHabovi != std::end(kod) && nadjiJuHabovi != std::end(kod))
					dist[i][j] = alfa * cene[i][j];
				else
					dist[i][j] = cene[i][j];

				/*
				If (gama != 1) and/or (delta != 1), the last else statement should be REPLACED with:
				else if (nadjiIuHabovi == std::end(kod))
					dist[i][j] = delta * cene[i][j];
				else
					dist[i][j] = gama * cene[i][j];
				*/

			}
		}
	}

	// Modified Floyd - Warshall algorithm to find costs of shortest paths via hubs
	// Time complexity: O(p*n^2), where n = brojCvorova
	void pronadji_najkrace_puteve(std::vector<std::vector<double> >& dist) {

		for (int brojac = 0; brojac < p; brojac++)
			for (int i = 0; i < brojCvorova; i++)
				for (int j = 0; j < brojCvorova; j++)
					dist[i][j] = std::min(dist[i][j], dist[i][kod[brojac]] + dist[kod[brojac]][j]);
	}

	// Time complexity: O(n^2), where n = brojCvorova
	double pronadji_max_cenu_najkracih_puteva(std::vector<std::vector<double> >& dist) {
		double max = 0;

		for (int i = 0; i < brojCvorova; i++)
			for (int j = 0; j < brojCvorova; j++)
				max = std::max(max, dist[i][j]);

		return max;
	}
	
};

std::vector<Jedinka> populacija;
std::vector<Jedinka> novaPopulacija;
int iteracija = 0;

int selekcija(int velicinaTurnira) {
	double min = POZITIVNA_BESKONACNOST;
	int pobednik = -1;
	
	for (int i = 0; i < velicinaTurnira; i++) {
		int k = std::rand() % VELICINA_POPULACIJE;
		
		if (populacija[k].fitnes < min) {
			min = populacija[k].fitnes;
			pobednik = k;
		}
	}
	
	return pobednik;
}

// ADJUST: Choose which function ukrstanje() should be used (3 options).
#if 0
// One-point crossover.
void ukrstanje(Jedinka& roditelj1, Jedinka& roditelj2, Jedinka& potomak1, Jedinka& potomak2) {
	int indeks = std::rand() % p;
	
	for (int i = 0; i < indeks; i++) {
		potomak1.kod[i] = roditelj1.kod[i];
		potomak2.kod[i] = roditelj2.kod[i];
	}
	
	for (int i = indeks; i < p; i++) {
		potomak1.kod[i] = roditelj1.kod[i];
		potomak2.kod[i] = roditelj2.kod[i];
	}
}
#endif

#if 0
// Two-point crossover.
void ukrstanje(Jedinka& roditelj1, Jedinka& roditelj2, Jedinka& potomak1, Jedinka& potomak2) {
	// Pick two random indexes.
	int prviIndeks = std::rand() % p;
	int drugiIndeks;
	do {
		drugiIndeks = std::rand() % p;
	} while (prviIndeks == drugiIndeks);

	// Store the picked values so that: prviIndeks < drugiIndeks.
	if (drugiIndeks < prviIndeks) {
		int pomocna = prviIndeks;
		prviIndeks = drugiIndeks;
		drugiIndeks = pomocna;
	}
	
	for (int i = 0; i < prviIndeks; i++) {
		potomak1.kod[i] = roditelj1.kod[i];
		potomak2.kod[i] = roditelj2.kod[i];
	}
	
	for (int i = prviIndeks; i < drugiIndeks; i++) {
		potomak1.kod[i] = roditelj2.kod[i];
		potomak2.kod[i] = roditelj1.kod[i];
	}

	for (int i = drugiIndeks; i < p; i++) {
		potomak1.kod[i] = roditelj1.kod[i];
		potomak2.kod[i] = roditelj2.kod[i];
	}
}
#endif

// Uniform crossover.
void ukrstanje(Jedinka& roditelj1, Jedinka& roditelj2, Jedinka& potomak1, Jedinka& potomak2) {
	
	for (int i = 0; i < p; i++) {
		int roditeljZaGenPotomka1 = std::rand() % 2;

		if (roditeljZaGenPotomka1 == 1) {
			potomak1.kod[i] = roditelj1.kod[i];
			potomak2.kod[i] = roditelj2.kod[i];
		} else {
			potomak1.kod[i] = roditelj2.kod[i];
			potomak2.kod[i] = roditelj1.kod[i];
		}
	}
}

void mutacija(Jedinka& jedinka) {
	int k;
	
	for (int i = 0; i < p; i++) {
		if (std::rand() % 100 > VEROVATNOCA_MUTACIJE)
			continue;
			
		do {
			k = std::rand() % brojCvorova;
		} while (std::find(jedinka.kod.begin(), jedinka.kod.end(), k) != jedinka.kod.end());
		jedinka.kod[i] = k;
	}
}

bool uporedi(const Jedinka&  i1, const Jedinka&  i2) {
	return i1.fitnes < i2.fitnes;
}

// Choose element from the neighborhood of the current solution. Replace one hub with a non-hub node.
std::pair<int, int> invertuj(Jedinka& jedinka) {
	int k = std::rand() % p;
	int j = jedinka.kod[k];
	
	int i;
	do {
		i = std::rand() % brojCvorova;
	} while (std::find(jedinka.kod.begin(), jedinka.kod.end(), i) != jedinka.kod.end());
	jedinka.kod[k] = i;
	
	return std::make_pair(j, k);
}

void vrati(int j, int k, Jedinka& jedinka) {
	jedinka.kod[k] = j;
}

// ADJUST: Choose which algorithm will be used for simulirano_kaljenje(). (2 options)
// ALgorithm 1.
double simulirano_kaljenje(Jedinka& jedinka,
							Stoperica& meriUkupnoVreme,
							std::ofstream& outputFajlSaVremenima) {
	Jedinka najboljaJedinka = jedinka;
	double trenutnaVrednost = jedinka.fitnes;				// Current value: f(H)
	double najboljaVrednost = trenutnaVrednost;				// f* = f(H)
	int iteracija = 0;

	double protekloVreme = meriUkupnoVreme.izracunaj_proteklo_vreme();
	while (iteracija++ < MAX_ITERACIJA_SA && protekloVreme < MAX_VREME_GA) {
		std::pair<int, int> par = invertuj(jedinka);		// Choose random solution H' in the neighborhood of H.
		int j = par.first;
		int k = par.second;
		
		double novaVrednost = jedinka.fitnes_funkcija();	// New value: f(H')
		if (novaVrednost < trenutnaVrednost)				// if (f(H') < f(H))
			trenutnaVrednost = novaVrednost;				//	f(H) = f(H')
		else {
			double zavesa = 1.0 / std::pow(iteracija, 0.5);
			double q = ((double) rand() / (RAND_MAX));
			
			if (zavesa > q)
				trenutnaVrednost = novaVrednost;
			else
				vrati(j, k, jedinka);	
		}
		
		if (novaVrednost < najboljaVrednost) {
			najboljaVrednost = novaVrednost;
			
			najboljaJedinka.kod = jedinka.kod;
			najboljaJedinka.fitnes = najboljaVrednost;
			
			outputFajlSaVremenima << std::fixed << std::setprecision(2) << "\tTrenutna najbolja vrednost: " << najboljaVrednost << "\t";
			double vremeTrazenjaNoveNajboljeVrednosti = meriUkupnoVreme.izracunaj_proteklo_vreme();
			outputFajlSaVremenima << "\n\tVreme: " << vremeTrazenjaNoveNajboljeVrednosti << " ms \n";
		}
		
		protekloVreme = meriUkupnoVreme.izracunaj_proteklo_vreme();
	}

	jedinka = najboljaJedinka;

	return najboljaVrednost;
}

#if 0
// Algorithm 2. With temperature.
double simulirano_kaljenje(Jedinka& jedinka,
							Stoperica& meriUkupnoVreme,
							std::ofstream& outputFajlSaVremenima) {
	Jedinka najboljaJedinka = jedinka;
	double trenutnaVrednost = jedinka.fitnes;				// Current value: f(H)
	double najboljaVrednost = trenutnaVrednost;				// f* = f(H)

	double protekloVreme = meriUkupnoVreme.izracunaj_proteklo_vreme();
	double temperatura = pocetnaTemperatura;
	while (temperatura > 0 /* min temperatura */ && protekloVreme < MAX_VREME_GA) {
		int brojPonavljanjaNaIstojTemp = 0;
		while (brojPonavljanjaNaIstojTemp < maxPonavljanjaNaIstojTemp && protekloVreme < MAX_VREME_GA) {
			std::pair<int, int> par = invertuj(jedinka);		// Choose random solution H' in the neighborhood of H.
			int j = par.first;
			int k = par.second;
		
			double novaVrednost = jedinka.fitnes_funkcija();	// New value: f(H')
			if (novaVrednost < trenutnaVrednost)				// if (f(H') < f(H))
				trenutnaVrednost = novaVrednost;				//	f(H) = f(H')
			else {
				double zavesa = exp((-1) * (novaVrednost - trenutnaVrednost) / temperatura);
				double q = ((double) rand() / (RAND_MAX));
				
				if (zavesa > q)
					trenutnaVrednost = novaVrednost;
				else
					vrati(j, k, jedinka);
			}
			
			if (novaVrednost < najboljaVrednost) {
				najboljaVrednost = novaVrednost;
				
				najboljaJedinka.kod = jedinka.kod;
				najboljaJedinka.fitnes = najboljaVrednost;

				outputFajlSaVremenima << std::fixed << std::setprecision(2) << "\tTrenutna najbolja vrednost: " << najboljaVrednost << "\t";
				double vremeTrazenjaNoveNajboljeVrednosti = meriUkupnoVreme.izracunaj_proteklo_vreme();
				outputFajlSaVremenima << "\n\tVreme: " << vremeTrazenjaNoveNajboljeVrednosti << " ms \n";
			}

			brojPonavljanjaNaIstojTemp++;
			protekloVremeSA = stoperica.izracunaj_proteklo_vreme();
			protekloVreme = meriUkupnoVreme.izracunaj_proteklo_vreme();
		}
		
		temperatura *= parametarHladjenja;
	}

	jedinka = najboljaJedinka;

	return najboljaVrednost;
}
#endif

void test(std::ifstream& fajlSaTestInstancama,
			tipInstance tipInstance,
			int brojInstanci,
			std::ofstream& outputFajlSaVremenima,
			std::ofstream& outputFajlSaHabovimaIMatricamaAlok,
			int brojPokretanjaNadJednomInstancom) {
	if (!brojInstanci)
		return;

	// Input and output comment from file "testInstance.txt"
	std::string instancaKomentar;
	getline(fajlSaTestInstancama, instancaKomentar);
	outputFajlSaVremenima << instancaKomentar << std::endl;

	int obradjenoInstanci = 0;
	while (obradjenoInstanci < brojInstanci) {
		std::string instanca;
		getline(fajlSaTestInstancama, instanca);

		if (tipInstance == CAB) {
			ucitajCAB(instanca.c_str(), fajlSaTestInstancama);
			fajlSaTestInstancama.ignore(); // Ignore new line character; only needed with CAB instances...see input file
		} else if (tipInstance == AP)
			ucitajAP(instanca.c_str());
		else if (tipInstance == URAND)
			ucitajURAND(instanca.c_str());
		else {
			std::cout << "Nekorektan unos.\n";
			return;
		}

		// ADJUST: Set desired time criteria.
		if (brojCvorova < 1000)
			MAX_VREME_GA = 1000; // Time in milliseconds.
		else
			MAX_VREME_GA = 1800000; // Time in milliseconds.

		outputFajlSaVremenima << "----------------------------- INSTANCA: " << instanca << " -----------------------------" << std::endl;
		outputFajlSaHabovimaIMatricamaAlok << "----------------------------- INSTANCA: " << instanca << " -----------------------------" << std::endl;
		for (int i = 0; i < brojPokretanjaNadJednomInstancom; i++) {
			outputFajlSaVremenima << "ITERACIJA " << i+1 << " od " << brojPokretanjaNadJednomInstancom << std::endl;
			outputFajlSaHabovimaIMatricamaAlok << "ITERACIJA " << i+1 << " od " << brojPokretanjaNadJednomInstancom << std::endl;

			// Next two lines are necessary because of the following for loop.
			// As soon as brojPokreatanjaNadJednomInstancom > 1 the push_back in the for loop won't be good. :)
			populacija.resize(0);
			novaPopulacija.resize(0);

			Stoperica meriUkupnoVreme;	// Start timer.
			
			// Generate initial generation.
			for (int i = 0; i < VELICINA_POPULACIJE; i++) {
				populacija.push_back(Jedinka());
				novaPopulacija.push_back(Jedinka());
			}
			std::sort(populacija.begin(), populacija.end(), uporedi);

			double najboljaVrednost = populacija[0].fitnes;
			outputFajlSaVremenima << std::fixed << std::setprecision(2) << "\tTrenutna najbojla vrednost: " << najboljaVrednost;
			double protekloVremeGA = meriUkupnoVreme.izracunaj_proteklo_vreme();
			outputFajlSaVremenima << "\n\tVreme: " << protekloVremeGA << " ms \n";

			// Only better solutions will replace populacija[0] with SA. "populacija" stays sorted. Update najboljaVrednost. 
			najboljaVrednost = simulirano_kaljenje(populacija[0], meriUkupnoVreme, outputFajlSaVremenima);

			int neparanBrojJedinkiZaUkrstanjeIMutaciju = (VELICINA_POPULACIJE - ELITA) % 2;
			while (protekloVremeGA < MAX_VREME_GA) {
				
				#if 0
				std::cout << "Generacija: " << iteracija << ", ";
				std::cout << std::fixed << std::setprecision(2) << "Fitnes " << populacija[0].fitnes << std::endl;
				#endif

				for (int i = 0; i < ELITA; i++)
					novaPopulacija[i] = populacija[i];

				// ADJUST: Adjust for loop. If there are an even number of individuals, i.e. (VELICINA_POPULACIJE - ELITA) % 2 == 0, on which genetic operators should be applied use the second loop.
				// TODO: Change this when you set the ELITE parameter. Delete "neparanBrojJedinkiZaUkrstanjeIMutaciju" parameter.
				// if (neparanBrojJedinkiZaUkrstanjeIMutaciju) {
					for (int i = ELITA; i < VELICINA_POPULACIJE-1; i+=2) {
						int i1 = selekcija(VELICINA_TURNIRA);
						int i2 = selekcija(VELICINA_TURNIRA);
						
						ukrstanje(populacija[i1], populacija[i2], novaPopulacija[i], novaPopulacija[i + 1]);
						
						mutacija(novaPopulacija[i]);
						mutacija(novaPopulacija[i + 1]);
						
						novaPopulacija[i].fitnes = novaPopulacija[i].fitnes_funkcija();
						novaPopulacija[i + 1].fitnes = novaPopulacija[i + 1].fitnes_funkcija();
					}

					mutacija(novaPopulacija[VELICINA_POPULACIJE-1]);
					novaPopulacija[VELICINA_POPULACIJE-1].fitnes = novaPopulacija[VELICINA_POPULACIJE-1].fitnes_funkcija();

				#if 0
				} else {
					for (int i = ELITA; i < VELICINA_POPULACIJE; i+=2) {
						int i1 = selekcija(VELICINA_TURNIRA);
						int i2 = selekcija(VELICINA_TURNIRA);
						
						ukrstanje(populacija[i1], populacija[i2], novaPopulacija[i], novaPopulacija[i + 1]);
						
						mutacija(novaPopulacija[i]);
						mutacija(novaPopulacija[i + 1]);
						
						novaPopulacija[i].fitnes = novaPopulacija[i].fitnes_funkcija();
						novaPopulacija[i + 1].fitnes = novaPopulacija[i + 1].fitnes_funkcija();
					}
				}
				#endif
				populacija = novaPopulacija;
				std::sort(populacija.begin(), populacija.end(), uporedi);

				if (populacija[0].fitnes < najboljaVrednost) {
					najboljaVrednost = populacija[0].fitnes;
					
					outputFajlSaVremenima << std::fixed << std::setprecision(2) << "\tTrenutna najbojla vrednost: " << najboljaVrednost;
					outputFajlSaVremenima << "\n\tVreme: " << meriUkupnoVreme.izracunaj_proteklo_vreme() << " ms \n";
				}

				// Only better solutions will replace populacija[0] with SA. "populacija" stays sorted. Update najboljaVrednost. 
				najboljaVrednost = simulirano_kaljenje(populacija[0], meriUkupnoVreme, outputFajlSaVremenima);

				protekloVremeGA = meriUkupnoVreme.izracunaj_proteklo_vreme();
			}

			outputFajlSaVremenima << "Ukupno vreme pre racunanja A i B matrice: " << meriUkupnoVreme.izracunaj_proteklo_vreme() << " ms";

			// Calculate allocation matrices.
			std::vector<std::vector<double> > dist;
			std::vector<std::vector<int> > A; // First allocation matrix.
			std::vector<std::vector<int> > B; // Second allocation matrix.
			inicijalizacija_pocetnih_matrica_alokacije(dist, 
													populacija[0].kod /* habovi */,
													A,
													B,
													alfa,
													cene,
													brojCvorova,
													POZITIVNA_BESKONACNOST);
			popuni_matrice_alokacije(dist,
									populacija[0].kod /* habovi */,
									p,
									A,
									B,
									brojCvorova,
									POZITIVNA_BESKONACNOST);

			outputFajlSaVremenima << "\nUkupno vreme racunajuci rad sa AB: " << meriUkupnoVreme.izracunaj_proteklo_vreme() << " ms\n\n";
			
			ispisi_vrednost_resenja_i_habove(outputFajlSaHabovimaIMatricamaAlok,
											populacija[0].fitnes /* vrednostResenja */,
											populacija[0].kod /* habovi */,
											p);
			ispisi_matrice_alokacije(outputFajlSaHabovimaIMatricamaAlok, A , B, brojCvorova, POZITIVNA_BESKONACNOST);

			outputFajlSaHabovimaIMatricamaAlok << std::endl;
		}

		obradjenoInstanci++;
	}

	fajlSaTestInstancama.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // Skip empty line in input file
}

int main(int argc, char** argv) {
	srand(time(NULL));

	// ADJUST: Adjust file paths.
	std::ifstream fajlSaTestInstancama("testInstances-small.txt");
	std::ofstream outputFajlSaVremenima("rezultati-hybrid-small.txt");
	std::ofstream outputFajlSaHabovimaIMatricamaAlok("rezultati-hybrid-saMatricama-small.txt");

	// ADJUST: Adjust number of runs for each instance.
	int brojPokretanjaNadJednomInstancom = 10;

	//ADJUST: Adjust third argument of function test().
	// CAB
	test(fajlSaTestInstancama,
			CAB /* tipInstance */,
			12 /* brojInstanci */,
			outputFajlSaVremenima,
			outputFajlSaHabovimaIMatricamaAlok,
			brojPokretanjaNadJednomInstancom);
	// AP
	test(fajlSaTestInstancama,
			AP /* tipInstance */,
			8 /* brojInstanci */,
			outputFajlSaVremenima,
			outputFajlSaHabovimaIMatricamaAlok,
			brojPokretanjaNadJednomInstancom);
	// URAND
	test(fajlSaTestInstancama,
			URAND /* tipInstance */,
			0 /* brojInstanci */,
			outputFajlSaVremenima,
			outputFajlSaHabovimaIMatricamaAlok,
			brojPokretanjaNadJednomInstancom);

	fajlSaTestInstancama.close();
  	outputFajlSaVremenima.close();
	outputFajlSaHabovimaIMatricamaAlok.close();

	return 0;
}
