#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <iomanip>
#include <algorithm>
#include <chrono>
#include "../includes/typesOfInstances.h"
#include "../includes/timer.h"
#include "../includes/allocationMatrices.h"
#include "../includes/outputFunctions.h"

constexpr int POZITIVNA_BESKONACNOST = 1e9;

// ADJUST: Adjust parameter values.
double MAX_VREME_SA;
#if 0	// Use in case Algorithm 2 is being used for SA. Adjust parameter values.
constexpr int maxPonavljanjaNaIstojTemp = 4097;
constexpr double pocetnaTemperatura = 43482.6461;
constexpr double parametarHladjenja = 0.4709;
#endif

int brojCvorova;
std::vector<std::vector<double> > cene;
int p;
// Set of hubs. In the GA this is used as the code of an individual.
std::vector<int> habovi;
std::vector<int> haboviNajbolje;
double alfa;
// If gama and delta are not equal to 1, include appropriate declaration
// double gama = 1, delta = 1;

#if 0
class Stoperica {
public:
	std::ofstream& m_outputFajlSaVremenima;

private:
	std::chrono::time_point<std::chrono::high_resolution_clock> m_pocetniTrenutak;

public:
	Stoperica(std::ofstream& outputFajlSaVremenima) : m_outputFajlSaVremenima(outputFajlSaVremenima) {
		m_pocetniTrenutak = std::chrono::high_resolution_clock::now();
	}

	~Stoperica() {
		zaustavi();
	}

	void zaustavi() {
		auto krajnjiTrenutak = std::chrono::high_resolution_clock::now();

		auto pocetak = std::chrono::time_point_cast<std::chrono::microseconds>(m_pocetniTrenutak).time_since_epoch().count();
		auto kraj = std::chrono::time_point_cast<std::chrono::microseconds>(krajnjiTrenutak).time_since_epoch().count();

		auto trajanje = kraj - pocetak;
		double ms = trajanje * 0.001;

		// std::cout << "Vreme: " << trajanje << " us, " << ms << " ms \n";
		m_outputFajlSaVremenima << std::fixed << std::setprecision(2) << "\t\t\tVreme: " << trajanje << " us, " << ms << " ms";
	}
};
#endif


void ucitajCAB(const char* nazivFajla, std::ifstream& fajlSaTestInstancama) {
	/*std::cout << "\nUnesite broj cvorova: ";
	std::cin >> brojCvorova;
	
	std::cout << "Unesite broj habova: ";
	std::cin >> p;

	std::cout << "Unesite vrednost za alfa: ";
	std::cin >> alfa;*/

	fajlSaTestInstancama >> brojCvorova;
	fajlSaTestInstancama >> p;
	fajlSaTestInstancama >> alfa;
		
	std::ifstream ulaz;
	ulaz.open(nazivFajla);
	
	cene.resize(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		cene[i].resize(brojCvorova);
		for (int j = 0; j < brojCvorova; j++) {
            // Ignore everything up to the first '.', or ulaz.ignore(15); would also work. See the CAB instances files. :)
			ulaz.ignore(std::numeric_limits<std::streamsize>::max(), '.');
			ulaz >> cene[i][j];
		}
	}
		
	ulaz.close();
}

void ucitajAP(const char* nazivFajla) {
	alfa = 0.75;

	std::ifstream ulaz;
	ulaz.open(nazivFajla);
		
	ulaz >> brojCvorova;

	std::vector<double> x, y;
	x.reserve(brojCvorova);
	y.reserve(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		ulaz >> x[i];
		ulaz >> y[i];
	}

	cene.resize(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		cene[i].resize(brojCvorova);
		for (int j = 0; j < brojCvorova; j++) {
			cene[i][j] = std::sqrt(std::pow(x[i] - x[j], 2) + std::pow(y[i] - y[j], 2));
		}
	}

	ulaz.ignore();	// Ignore new line character
	//Ignore next brojCvorova lines.
	for (int i = 0; i < brojCvorova; i++)
		ulaz.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	ulaz >> p;
		
	ulaz.close();
}

void ucitajURAND(const char* nazivFajla) {
	alfa = 0.75;

	std::ifstream ulaz;
	ulaz.open(nazivFajla);
		
	ulaz >> brojCvorova;
		
	std::vector<double> x, y;
	x.reserve(brojCvorova);
	y.reserve(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		ulaz >> x[i];
		ulaz >> y[i];
	}
	
	cene.resize(brojCvorova);
	for (int i = 0; i < brojCvorova; i++) {
		cene[i].resize(brojCvorova);
		for (int j = 0; j < brojCvorova; j++) {
			cene[i][j] = std::sqrt(std::pow(x[i] - x[j], 2) + std::pow(y[i] - y[j], 2));
		}
	}

	ulaz.ignore();	// Ignore new line character
	// Ignore next brojCvorova lines.
	for (int i = 0; i < brojCvorova; i++)
		ulaz.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	ulaz >> p;
		
	ulaz.close();
}

void inicijalizuj() {
	std::vector<double> maxTrosakZaDatiCvor;
	maxTrosakZaDatiCvor.resize(brojCvorova);
	habovi.resize(p);
	std::vector<std::pair<double, int> > parIndeksVr;
	
	// For each node h find maximum cost of transport, considering all costs form h to every other node.
	// That is, find max(chj), where j goes through the set of nodes.
	for (int i = 0; i < brojCvorova; i++) {
			maxTrosakZaDatiCvor[i] = cene[i][0];
		for (int j = 1; j < brojCvorova; j++) {
			if (maxTrosakZaDatiCvor[i] < cene[i][j])
				maxTrosakZaDatiCvor[i] = cene[i][j];
		}
		
		parIndeksVr.push_back(std::make_pair(maxTrosakZaDatiCvor[i], i));
	}
	
	// Sort the array of the maximum costs in ascending order. Keep track of indexes of nodes.
	std::sort(parIndeksVr.begin(), parIndeksVr.end());
	
	// Take p nodes with the minimal previously found maximal costs. Set those p nodes to be hubs.
	for (int i = 0; i < p; i++) {
		habovi[i] = parIndeksVr[i].second;
		//std::cout << habovi[i] << "\n";
	}
}

// Time complexity: O(n^2*p), where n = brojCvorova.
void inicijalizacija_pocetne_matrice(std::vector<std::vector<double> >& dist) {
    /*
    In this case gama = 1 and delta = 1. Constructing the initial dist matrix:
    dist[i][j] = infinity,              i, j are not hubs                       (neither)
    dist[i][j] = alfa * cene[i][j],     i and j are both hubs                   (both)
    dist[i][j] = cene[i][j],            i != j, exactly one of i, j is a hub    (exactly one)
    */
    /*
    If (gama != 1) and/or (delta != 1), constructing the initial dist matrix:
    dist[i][j] = infinity,                 i, j are not hubs                   (neither)
    dist[i][j] = alfa * cene[i][j],        i and j are both hubs               (both)
    dist[i][j] = delta * cene[i][j],       when i is a hub                     (only i is a hub)
    dist[i][j] = gama * cene[i][j],        when j is a hub                     (only j is a hub)
   */

	dist.resize(brojCvorova);
	for (int i = 0; i < brojCvorova; i++){
		dist[i].resize(brojCvorova);
		for (int j = 0; j < brojCvorova; j++){
            auto nadjiIuHabovi = std::find(std::begin(habovi), std::end(habovi), i);
            auto nadjiJuHabovi = std::find(std::begin(habovi), std::end(habovi), j);

            if (nadjiIuHabovi == std::end(habovi) && nadjiJuHabovi == std::end(habovi))
                dist[i][j] = POZITIVNA_BESKONACNOST;
            else if (nadjiIuHabovi != std::end(habovi) && nadjiJuHabovi != std::end(habovi))
                dist[i][j] = alfa * cene[i][j];
            else
                dist[i][j] = cene[i][j];

            /*
            If (gama != 1) and/or (delta != 1), the last else statement should be REPLACED with:
            else if (nadjiIuHabovi == std::end(habovi))
                dist[i][j] = delta * cene[i][j];
            else
                dist[i][j] = gama * cene[i][j];
            */

		}
	}
}

// Modified Floyd - Warshall algorithm to find costs of shortest paths via hubs.
// Time complexity: O(p*n^2), where n = brojCvorova
void pronadji_najkrace_puteve(std::vector<std::vector<double> >& dist) {

	for (int brojac = 0; brojac < p; brojac++)
		for (int i = 0; i < brojCvorova; i++)
			for (int j = 0; j < brojCvorova; j++)
					dist[i][j] = std::min(dist[i][j], dist[i][habovi[brojac]] + dist[habovi[brojac]][j]);
}

// Time complexity: O(n^2), where n = brojCvorova.
double pronadji_max_cenu_najkracih_puteva(std::vector<std::vector<double> >& dist) {
    double max = 0;

	for (int i = 0; i < brojCvorova; i++)
		for (int j = 0; j < brojCvorova; j++)
				max = std::max(max, dist[i][j]);

	return max;
}

double vrednost_resenja() {
	std::vector<std::vector<double> > dist;

	inicijalizacija_pocetne_matrice(dist);
	pronadji_najkrace_puteve(dist);
	double max = pronadji_max_cenu_najkracih_puteva(dist);

	return max;
}

// Choose element from the neighborhood of the current solution. Replace one hub with a non-hub node.
std::pair<int, int> invertuj() {
	int k = std::rand() % p;
	int j = habovi[k];
	
	int i;
	do {
		i = std::rand() % brojCvorova;
	} while (std::find(habovi.begin(), habovi.end(), i) != habovi.end());
	habovi[k] = i;
	
	return std::make_pair(j, k);
}

void vrati(int j, int k) {
	habovi[k] = j;
}

// ADJUST: Choose which algorithm will be used for simulirano_kaljenje(). (2 options)
// ALgorithm 1.
double simulirano_kaljenje(std::ofstream& outputFajlSaVremenima, Stoperica& stoperica) {
	double trenutnaVrednost = vrednost_resenja();	// Current value: f(H)
	double najboljaVrednost = trenutnaVrednost;		// f* = f(H)
	int iteracija = 0;
	
	haboviNajbolje.resize(p);

	double protekloVremeSA = stoperica.izracunaj_proteklo_vreme();
	while (protekloVremeSA < MAX_VREME_SA /* && iteracija++ < MAX_ITERACIJA*/) {
		std::pair<int, int> par = invertuj();		// Choose random solution H' in the neighborhood of H.
		int j = par.first;
		int k = par.second;
		
		double novaVrednost = vrednost_resenja();	// New value: f(H')
		if (novaVrednost < trenutnaVrednost)		// if (f(H') < f(H))
			trenutnaVrednost = novaVrednost;		// f(H) = f(H')
		else {
			double zavesa = 1.0 / std::pow(iteracija, 0.5);
			
			double q = ((double) rand() / (RAND_MAX));
			
			if (zavesa > q)
				trenutnaVrednost = novaVrednost;
			else 
				vrati(j, k);
			
		}
		
		if (novaVrednost < najboljaVrednost) {
			najboljaVrednost = novaVrednost;
			haboviNajbolje = habovi;

			// TODO "time": take time here and see time when the best solution was found.
			outputFajlSaVremenima << std::fixed << std::setprecision(2) << "\tTrenutna najbolja vrednost: " << najboljaVrednost;
			double vremeTrazenjaNoveNajboljeVrednosti = stoperica.izracunaj_proteklo_vreme();
			outputFajlSaVremenima << "\n\tVreme: " << vremeTrazenjaNoveNajboljeVrednosti << " ms \n";
		}

		protekloVremeSA = stoperica.izracunaj_proteklo_vreme();
	}
	
	return najboljaVrednost;
}

#if 0
// Algorithm 2. See science paper.
double simulirano_kaljenje_algoritam2(std::ofstream& outputFajlSaVremenima, Stoperica& stoperica) {
	double trenutnaVrednost =   vrednost_resenja();	// Current value: f(H)
	double najboljaVrednost = trenutnaVrednost;					// f* = f(H)
	
	haboviNajbolje.resize(p);

	double temperatura = pocetnaTemperatura;
	double protekloVremeSA = stoperica.izracunaj_proteklo_vreme();
	while (temperatura > 0) {
		int brojPonavljanjaNaIstojTemp = 0;
		while (brojPonavljanjaNaIstojTemp < maxPonavljanjaNaIstojTemp) {
			std::pair<int, int> par = invertuj();		// Choose random solution H' in the neighborhood of H.
			int j = par.first;
			int k = par.second;
			
			double novaVrednost = vrednost_resenja();	// New value: f(H')
			if (novaVrednost < trenutnaVrednost)					// if (f(H') < f(H))
				trenutnaVrednost = novaVrednost;					// f(H) = f(H')
			else {
				double zavesa = exp((-1) * (novaVrednost - trenutnaVrednost) / temperatura);
				double q = ((double) rand() / (RAND_MAX));
				
				if (zavesa > q)
					trenutnaVrednost = novaVrednost;
				else 
					vrati(j, k);
				
			}
			
			if (novaVrednost < najboljaVrednost) {
				najboljaVrednost = novaVrednost;
				haboviNajbolje = habovi;			
				
				outputFajlSaVremenima << "Trenutna najbolja vrednost: " << najboljaVrednost << "\t";
				double vremeTrazenjaNoveNajboljeVrednosti = stoperica.izracunaj_proteklo_vreme();
				outputFajlSaVremenima << "Vreme: " << vremeTrazenjaNoveNajboljeVrednosti << " ms \n";
			}
			
			brojPonavljanjaNaIstojTemp++;
			protekloVremeSA = stoperica.izracunaj_proteklo_vreme();
		}
		
		temperatura *= parametarHladjenja;
	}

	return najboljaVrednost;
}
#endif

void test(std::ifstream& fajlSaTestInstancama,
			tipInstance tipInstance,
			int brojInstanci,
			std::ofstream& outputFajlSaVremenima,
			std::ofstream& outputFajlSaHabovimaIMatricamaAlok,
			int brojPokretanjaNadJednomInstancom) {
	if (!brojInstanci)
		return;

	// Input and output comment from file "testInstance.txt"
	std::string instancaKomentar;
	getline(fajlSaTestInstancama, instancaKomentar);
	outputFajlSaVremenima << instancaKomentar << std::endl;

	int obradjenoInstanci = 0;
	while (obradjenoInstanci < brojInstanci) {
		std::string instanca;
		getline(fajlSaTestInstancama, instanca);

		if (tipInstance == CAB) {
			ucitajCAB(instanca.c_str(), fajlSaTestInstancama);
			fajlSaTestInstancama.ignore(); // Ignore new line character; only needed with CAB instances...see input file
		} else if (tipInstance == AP)
			ucitajAP(instanca.c_str());
		else if (tipInstance == URAND)
			ucitajURAND(instanca.c_str());
		else {
			std::cout << "Nekorektan unos.\n";
			return;
		}

		// ADJUST: Set desired time criteria.
		if (brojCvorova < 1000)
			MAX_VREME_SA = 1000; // Time in milliseconds.
		else
			MAX_VREME_SA = 1800000; // Time in milliseconds.

		outputFajlSaVremenima << "----------------------------- INSTANCA: " << instanca << " -----------------------------" << std::endl;
		outputFajlSaHabovimaIMatricamaAlok << "----------------------------- INSTANCA: " << instanca << " -----------------------------" << std::endl;
		for (int i = 0; i < brojPokretanjaNadJednomInstancom; i++) {
			outputFajlSaVremenima << "ITERACIJA " << i+1 << " od " << brojPokretanjaNadJednomInstancom << std::endl;
			outputFajlSaHabovimaIMatricamaAlok << "ITERACIJA " << i+1 << " od " << brojPokretanjaNadJednomInstancom << std::endl;

			Stoperica meriUkupnoVreme;

			inicijalizuj();
			double vrednostResenja = simulirano_kaljenje(outputFajlSaVremenima, meriUkupnoVreme /* stoperica */);

			double protekloVremePreAB = meriUkupnoVreme.izracunaj_proteklo_vreme();	// Stop timer;
			outputFajlSaVremenima << "Ukupno vreme pre racunanja A i B matrice: " << protekloVremePreAB << " ms";

			// Calculate allocation matrices.
			std::vector<std::vector<double> > dist;
			std::vector<std::vector<int> > A; // First allocation matrix.
			std::vector<std::vector<int> > B; // Second allocation matrix.
			inicijalizacija_pocetnih_matrica_alokacije(dist, 
													haboviNajbolje,
													A,
													B,
													alfa,
													cene,
													brojCvorova,
													POZITIVNA_BESKONACNOST);
			popuni_matrice_alokacije(dist,
									haboviNajbolje,
									p,
									A,
									B,
									brojCvorova,
									POZITIVNA_BESKONACNOST);

			double ukupnoVreme = meriUkupnoVreme.izracunaj_proteklo_vreme();
			outputFajlSaVremenima << "\nUkupno vreme racunajuci rad sa AB: " << ukupnoVreme << " ms\n\n";

			
			ispisi_vrednost_resenja_i_habove(outputFajlSaHabovimaIMatricamaAlok, vrednostResenja, haboviNajbolje, p);
			ispisi_matrice_alokacije(outputFajlSaHabovimaIMatricamaAlok, A , B, brojCvorova, POZITIVNA_BESKONACNOST);
			outputFajlSaHabovimaIMatricamaAlok << std::endl;
		}

		obradjenoInstanci++;
	}

	fajlSaTestInstancama.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // Skip empty line in input file
}

int main() {
	srand(time(NULL));

	// ADJUST: Adjust file paths.
	std::ifstream fajlSaTestInstancama("testInstances-small.txt");
	std::ofstream outputFajlSaVremenima("rezultati-SA-small.txt");
	std::ofstream outputFajlSaHabovimaIMatricamaAlok("rezultati-SA-saMatricama-small.txt");

	// ADJUST: Adjust number of runs for each instance.
	int brojPokretanjaNadJednomInstancom = 5;
	
	//ADJUST: Adjust third argument of function test().
	// CAB
	test(fajlSaTestInstancama,
			CAB /* tipInstance */,
			12 /* brojInstanci */,
			outputFajlSaVremenima,
			outputFajlSaHabovimaIMatricamaAlok,
			brojPokretanjaNadJednomInstancom);
	// AP
	test(fajlSaTestInstancama,
			AP /* tipInstance */,
			8 /* brojInstanci */,
			outputFajlSaVremenima,
			outputFajlSaHabovimaIMatricamaAlok,
			brojPokretanjaNadJednomInstancom);
	// URAND
	test(fajlSaTestInstancama,
			URAND /* tipInstance */,
			0 /* brojInstanci */,
			outputFajlSaVremenima,
			outputFajlSaHabovimaIMatricamaAlok,
			brojPokretanjaNadJednomInstancom);

	fajlSaTestInstancama.close();
  	outputFajlSaVremenima.close();
	outputFajlSaHabovimaIMatricamaAlok.close();

	return 0;
}
