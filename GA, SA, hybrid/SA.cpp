#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <algorithm>
#include <iomanip>
#include "includes/typesOfInstances.h"
#include "includes/inputFunctions.h"
#include "includes/timer.h"
#include "includes/allocationMatrices.h"
#include "includes/outputFunctions.h"

constexpr int POZITIVNA_BESKONACNOST = 1e9;

// ADJUST: Adjust parameter values.
constexpr double MAX_VREME_SA = 1000;
#if 0	// Use in case Algorithm 2 is being used for SA. Adjust parameter values.
constexpr double pocetnaTemperatura = 43482.6461;
constexpr double parametarHladjenja = 0.4709;	
constexpr double maxPonavljanjaNaIstojTemp = 4097;
#endif

int brojCvorova;
std::vector<std::vector<double> > cene;
int p;
std::vector<int> habovi; // Set of hubs. In the GA this is used as the code of an individual.
std::vector<int> haboviNajbolje;
double alfa;
// If gama and delta are not equal to 1, include appropriate declaration
// double gama = 1, delta = 1;

void inicijalizuj() {
	std::vector<double> maxTrosakZaDatiCvor;
	maxTrosakZaDatiCvor.resize(brojCvorova);
	habovi.resize(p);
	std::vector<std::pair<double, int> > parIndeksVr;
	
	// For each node h find maximum cost of transport, considering all costs form h to every other node.
	// That is, find max(chj), where j goes through the set of nodes.
	for (int i = 0; i < brojCvorova; i++) {
			maxTrosakZaDatiCvor[i] = cene[i][0];
		for (int j = 1; j < brojCvorova; j++) {
			maxTrosakZaDatiCvor[i] = std::max(maxTrosakZaDatiCvor[i], cene[i][j]);
		}
		
		parIndeksVr.push_back(std::make_pair(maxTrosakZaDatiCvor[i], i));
	}
	
	// Sort the array of the maximum costs in ascending order. Keep track of indexes of nodes.
	std::sort(parIndeksVr.begin(), parIndeksVr.end());
	
	// Take p nodes with the minimal previously found maximal costs. Set those p nodes to be hubs.
	for (int i = 0; i < p; i++) {
		habovi[i] = parIndeksVr[i].second;
		//std::cout << habovi[i] << "\n";
	}
}

// Time complexity: O(n^2*p), where n = brojCvorova.
void inicijalizacija_pocetne_matrice(std::vector<std::vector<double> >& dist) {
    /*
    In this case gama = 1 and delta = 1. Constructing the initial dist matrix:
    dist[i][j] = infinity,              i, j are not hubs                       (neither)
    dist[i][j] = alfa * cene[i][j],     i and j are both hubs                   (both)
    dist[i][j] = cene[i][j],            i != j, exactly one of i, j is a hub    (exactly one)
    */
    /*
    If (gama != 1) and/or (delta != 1), constructing the initial dist matrix:
    dist[i][j] = infinity,                 i, j are not hubs                   (neither)
    dist[i][j] = alfa * cene[i][j],        i and j are both hubs               (both)
    dist[i][j] = delta * cene[i][j],       when i is a hub                     (only i is a hub)
    dist[i][j] = gama * cene[i][j],        when j is a hub                     (only j is a hub)
   */

	dist.resize(brojCvorova);
	for (int i = 0; i < brojCvorova; i++){
		dist[i].resize(brojCvorova);
		for (int j = 0; j < brojCvorova; j++){
            auto nadjiIuHabovi = std::find(std::begin(habovi), std::end(habovi), i);
            auto nadjiJuHabovi = std::find(std::begin(habovi), std::end(habovi), j);

            if (nadjiIuHabovi == std::end(habovi) && nadjiJuHabovi == std::end(habovi))
                dist[i][j] = POZITIVNA_BESKONACNOST;
            else if (nadjiIuHabovi != std::end(habovi) && nadjiJuHabovi != std::end(habovi))
                dist[i][j] = alfa * cene[i][j];
            else
                dist[i][j] = cene[i][j];

			// If (gama != 1) and/or (delta != 1), the last else statement should be REPLACED with:
            #if 0
            else if (nadjiIuHabovi == std::end(habovi))
                dist[i][j] = delta * cene[i][j];
            else
                dist[i][j] = gama * cene[i][j];
            #endif

		}
	}
}

// Modified Floyd - Warshall algorithm to find costs of shortest paths via hubs.
// Time complexity: O(p*n^2), where n = brojCvorova
void pronadji_najkrace_puteve(std::vector<std::vector<double> >& dist) {

	for (int brojac = 0; brojac < p; brojac++)
		for (int i = 0; i < brojCvorova; i++)
			for (int j = 0; j < brojCvorova; j++)
					dist[i][j] = std::min(dist[i][j], dist[i][habovi[brojac]] + dist[habovi[brojac]][j]);
}

// Time complexity: O(n^2), where n = brojCvorova.
double pronadji_max_cenu_najkracih_puteva(std::vector<std::vector<double> >& dist) {
    double max = 0;

	for (int i = 0; i < brojCvorova; i++)
		for (int j = 0; j < brojCvorova; j++)
				max = std::max(max, dist[i][j]);

	return max;
}

double vrednost_resenja() {
	std::vector<std::vector<double> > dist;

	inicijalizacija_pocetne_matrice(dist);
	pronadji_najkrace_puteve(dist);
	double max = pronadji_max_cenu_najkracih_puteva(dist);

	return max;
}

// Choose element from the neighborhood of the current solution. Replace one hub with a non-hub node.
std::pair<int, int> invertuj() {
	int k = std::rand() % p;
	int j = habovi[k];
	
	int i;
	do {
		i = std::rand() % brojCvorova;
	} while (std::find(habovi.begin(), habovi.end(), i) != habovi.end());
	habovi[k] = i;
	
	return std::make_pair(j, k);
}

void vrati(int j, int k) {
	habovi[k] = j;
}

// ADJUST: Choose which algorithm will be used for simulirano_kaljenje(). (2 options)
// Algorithm 1. See science paper.
double simulirano_kaljenje(Stoperica& stoperica) {
	double trenutnaVrednost = vrednost_resenja();	// Current value: f(H)
	double najboljaVrednost = trenutnaVrednost;		// f* = f(H)
	int iteracija = 0;
	
	haboviNajbolje.resize(p); // TODO: Make this better. Maybe not global but return with this function, using a struct called solution.

	double protekloVremeSA = stoperica.izracunaj_proteklo_vreme();
	while (protekloVremeSA < MAX_VREME_SA) {
		std::pair<int, int> par = invertuj();		// Choose random solution H' in the neighborhood of H.
		int j = par.first;
		int k = par.second;
		
		double novaVrednost = vrednost_resenja();	// New value: f(H')
		if (novaVrednost < trenutnaVrednost)		// if (f(H') < f(H))
			trenutnaVrednost = novaVrednost;		// f(H) = f(H')
		else {
			double zavesa = 1.0 / std::pow(iteracija, 0.5);
			#if 0
			double zavesa = log(2) / log(1 + iteracija);
			#endif

			double q = ((double) rand() / (RAND_MAX));
			
			if (zavesa > q)
				trenutnaVrednost = novaVrednost;
			else 
				vrati(j, k);
			
		}
		
		if (novaVrednost < najboljaVrednost) {
			najboljaVrednost = novaVrednost;
			haboviNajbolje = habovi;

			std::cout << "Trenutna najbolja vrednost: " << std::fixed << std::setprecision(2) << najboljaVrednost << "\t";
			double vremeTrazenjaNoveNajboljeVrednosti = stoperica.izracunaj_proteklo_vreme();
			std::cout << "Vreme: " << vremeTrazenjaNoveNajboljeVrednosti << " ms \n";
		}

		protekloVremeSA = stoperica.izracunaj_proteklo_vreme();
	}
	
	return najboljaVrednost;
}

#if 0
// Algorithm 2. See science paper.
double simulirano_kaljenje(Stoperica& stoperica) {
	double trenutnaVrednost = vrednost_resenja();	// Current value: f(H)
	double najboljaVrednost = trenutnaVrednost;		// f* = f(H)
	
	haboviNajbolje.resize(p); // TODO: Make this better. Maybe not global but return with this function, using a struct.

	double temperatura = pocetnaTemperatura;
	double protekloVremeSA = stoperica.izracunaj_proteklo_vreme();
	while (temperatura > 0) {
		int brojPonavljanjaNaIstojTemp = 0;
		while (brojPonavljanjaNaIstojTemp < maxPonavljanjaNaIstojTemp) {
			std::pair<int, int> par = invertuj();		// Choose random solution H' in the neighborhood of H.
			int j = par.first;
			int k = par.second;
			
			double novaVrednost = vrednost_resenja();	// New value: f(H')
			if (novaVrednost < trenutnaVrednost)		// if (f(H') < f(H))
				trenutnaVrednost = novaVrednost;		// f(H) = f(H')
			else {
				double zavesa = exp((-1) * (novaVrednost - trenutnaVrednost) / temperatura);
				double q = ((double) rand() / (RAND_MAX));
				
				if (zavesa > q)
					trenutnaVrednost = novaVrednost;
				else 
					vrati(j, k);
				
			}
			
			if (novaVrednost < najboljaVrednost) {
				najboljaVrednost = novaVrednost;
				haboviNajbolje = habovi;

				std::cout << "Trenutna najbolja vrednost: " << std::fixed << std::setprecision(2) << najboljaVrednost << "\t";
				double vremeTrazenjaNoveNajboljeVrednosti = stoperica.izracunaj_proteklo_vreme();
				std::cout << "Vreme: " << vremeTrazenjaNoveNajboljeVrednosti << " ms \n";
			}
			
			brojPonavljanjaNaIstojTemp++;
			protekloVremeSA = stoperica.izracunaj_proteklo_vreme();
		}
		
		temperatura *= parametarHladjenja;
	}

	return najboljaVrednost;
}
#endif

int main(int argc, char** argv) {
	unsigned instance;
	
	srand(time(NULL));

	std::cout << "\nUnesite broj 1 ako se program pokrece nad CAB instancama, broj 2 ukoliko se pokrece nad AP instancama ili broj 3 ukoliko su u pitanju URAND instance: ";
	std::cin >> instance;
	std::cout << "\n";

	static_cast<tipInstance>(instance);

	// Read instance file.
	if (instance == CAB) 
		ucitajCAB(argv[1], brojCvorova, p, alfa, cene);
	else if (instance == AP)
		ucitajAP(argv[1], brojCvorova, p, alfa, cene);
	else if (instance == URAND)
		ucitajURAND(argv[1], brojCvorova, p, alfa, cene);
	else {
		std::cout << "Nekorektan unos.\n";
		return 0;
	}

	Stoperica stopericaSA;	// Start timer.

	inicijalizuj();
	double vrednostResenja = simulirano_kaljenje(stopericaSA);

	// Output time spent, NOT including time for calculating allocation matrices.
	double protekloVremePreAB = stopericaSA.izracunaj_proteklo_vreme();
	std::cout << "Ukupno vreme pre racunanja A i B matrice: " << protekloVremePreAB << " ms \n";

	// Calculate allocation matrices.
	std::vector<std::vector<double> > dist;
	std::vector<std::vector<int> > A; // First allocation matrix.
	std::vector<std::vector<int> > B; // Second allocation matrix.
	inicijalizacija_pocetnih_matrica_alokacije(dist, 
											haboviNajbolje,
											A,
											B,
											alfa,
											cene,
											brojCvorova,
											POZITIVNA_BESKONACNOST);
	popuni_matrice_alokacije(dist,
							haboviNajbolje,
							p,
							A,
							B,
							brojCvorova,
							POZITIVNA_BESKONACNOST);

	// Output time spent, including time for calculating allocation matrices.
	double protekloVreme = stopericaSA.izracunaj_proteklo_vreme();
	std::cout << "Ukupno vreme: " << protekloVreme << " ms \n";

	// Output results.
	std::ofstream outputFajl("results/SA/small_instances/AP102-rezultati.txt");	// ADJUST: Adjust output file name.
	ispisi_vrednost_resenja_i_habove(outputFajl, vrednostResenja, haboviNajbolje, p);
	ispisi_matrice_alokacije(outputFajl, A , B, brojCvorova, POZITIVNA_BESKONACNOST);
	outputFajl.close();

	return 0;
}
